

#ifndef _FUNCTIONS_H_
#define _FUNCTIONS_H_


bool sleepUs( int msec );
void set_fl(int fd, int flags);
void clr_fl(int fd, int flags);
#ifdef DEBUG
    void debug( const char* format, ... );
#endif

#endif // _FUNCTIONS_H_