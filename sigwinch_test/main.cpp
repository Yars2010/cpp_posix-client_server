

#include <stdio.h>
#include <stdlib.h>
#include <termios.h>
#include <signal.h>
#include <sys/ioctl.h>
#include <unistd.h>


static void pr_winsize( int fd )
{
    struct winsize size;
    
    if( ioctl( fd, TIOCGWINSZ, (char*)&size ) < 0 )
        exit(1);
    printf( "%d rows, %d cols\n", size.ws_row, size.ws_col );
}


static void sig_winch( int signo )
{
    printf( "catch SIGWINCH signal\n" );
    pr_winsize( STDIN_FILENO );
}


int main(int argc, char **argv)
{
	if( isatty( STDIN_FILENO ) == 0 )
        exit(1);
    
    if( signal( SIGWINCH, sig_winch ) == SIG_ERR )
        exit(1);
    
    pr_winsize( STDIN_FILENO );
    
    for( ;; )
        pause();
}
