

#include <stdio.h>
#include <pthread.h>
#include <signal.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <unistd.h>


int quitflag;
sigset_t mask;


pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t wait = PTHREAD_COND_INITIALIZER;


void* thr_fn( void *arg )
{
    int err, signo;
    
    for(;;)
    {
        err = sigwait( &mask, &signo );
        if( err != 0 )
            exit(1);
        
        switch( signo )
        {
        case SIGINT:
            pthread_mutex_lock( &lock );
            quitflag = 1;
            pthread_mutex_unlock( &lock );
            pthread_cond_signal( &wait );
            return (0);
            
        case SIGWINCH:
            struct winsize size;
            if( ioctl( STDIN_FILENO, TIOCGWINSZ, (char*)&size ) < 0 )
                exit(1);
            printf( "%d rows, %d cols\n", size.ws_row, size.ws_col );
            break;
        
        default:
            printf( "unknow signal %d\n", signo );
            exit(1);
        }
    }
}


int main(int argc, char **argv)
{
	int err;
    sigset_t oldmask;
    pthread_t tid;
    
    sigemptyset( &mask );
    sigaddset( &mask, SIGWINCH );
    sigaddset( &mask, SIGINT );
    
    if( ( err = pthread_sigmask( SIG_BLOCK, &mask, &oldmask) ) != 0 )
        exit(1);
    
    if( ( err = pthread_create( &tid, NULL, thr_fn, 0 ) ) != 0 )
        exit(1);
        
    pthread_mutex_lock( &lock );
    while( quitflag == 0 )
        pthread_cond_wait( &wait, &lock );
    pthread_mutex_unlock( &lock );
    
    quitflag = 0;
    
    printf( "%s\n", "thank's, goodbuy!" );
    
    if( sigprocmask( SIG_SETMASK, &oldmask, NULL ) < 0 )
        exit(1);
        
    return 0;
}
