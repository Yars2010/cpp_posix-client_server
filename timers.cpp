

#include "timers.h"
#include "lock_guard.h"
#include "functions.h"
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/time.h>
#include <limits.h>


class TimerQueue
{
public:
    struct Item
    {
        Timer* timer;
        Item* next;
    };
    
    TimerQueue();
    ~TimerQueue();
    
    void pushBack( Timer* timer );
    Timer* popFront( );
    int size() const;

private:
    Item *_head, *_tail;
    int _size;
    
};


TimerQueue::TimerQueue()
    : _head( nullptr ), _tail( nullptr ), _size(0)
{
}


TimerQueue::~TimerQueue()
{
    while( _size > 0 )
    {
        Timer* timer = popFront();
        delete timer;
    }
}


void TimerQueue::pushBack( Timer* timer )
{
    if( _tail != NULL)
    {
        Item *temp = new Item();
        
        temp->timer = timer;
        temp->next = nullptr;
        
        _tail->next = temp;
        _tail = temp;
    }
    else
    {
        _tail = new Item();
        
        _tail->timer = timer;
        _tail->next = nullptr;
        
        _head = _tail;
    }
    ++_size;
}


Timer* TimerQueue::popFront( )
{
    if ( _head != NULL )
    {
        Timer* timer = _head->timer;
        if( _head == _tail )
            _tail = NULL;
        
        Item* tmp = _head;
        
        _head = _head->next;
        --_size;
        
        delete tmp;
        
        return timer;
    }
    else
        return nullptr;
}


int TimerQueue::size() const
{
    return _size;
}


int Timer::_count = 0;


Timer::Timer( Timers* timers, handler func, void* arg1, void* arg2  )
    :   _timers(timers),
        _id(0), 
        _handled(false),
        _handling(false), 
        _deleted(false),
        _interval(0), 
        _value(0),        
        _repeat(false),
        _play(false),
        _func(func), 
        _arg1(arg1), 
        _arg2(arg2)
{
    if( _count == INT_MAX ) {
        _count = 0;
    }
    _id = _count++;
    _tid = pthread_self();

    sigset_t mask;
    sigemptyset( &mask );
    sigaddset( &mask, SIGVTALRM );
    pthread_sigmask( SIG_UNBLOCK, &mask, NULL );
}


Timer::~Timer()
{
}


void Timer::runHandler()
{
    if( _func ) {
        _func( _arg1, _arg2 );
    }
}


bool Timer::isPlay() const
{   
    LockGuard lock( &_timers->_mutex );
    return _play;
}


bool Timer::isStop() const
{
    LockGuard lock( &_timers->_mutex );
    return !_play;
}


int Timer::id() const
{
    LockGuard lock( &_timers->_mutex );
    return _id;
}


void Timer::setInterval( int msec_interval )
{
    LockGuard lock( &_timers->_mutex );
    _interval = msec_interval;
}


int Timer::interval() const
{
    LockGuard lock( &_timers->_mutex );
    return _interval;
}


void Timer::play( int interval, bool repeat )
{
    LockGuard lock( &_timers->_mutex );
    
    _play = true;
    _interval = interval;
    _value = interval;
    _repeat = repeat;
    
    _timers->startAlrmSignal();
}


void Timer::pause()
{
    LockGuard lock( &_timers->_mutex );
    
    _play = false;
    _timers->stopAlrmSignal();
}


void Timer::stop()
{
    LockGuard lock( &_timers->_mutex );
    
    _play = false;
    _value = _interval;
    _timers->stopAlrmSignal();
}


Timers* Timers::_instance = nullptr;


void Timers::handlingTimers()
{
    int err, signo, count = 0;
    while( true )
    {
        while( _queue->size() > 0 )
        {              
            _current_timer = _queue->popFront();           
            pthread_kill( _current_timer->_tid, SIGVTALRM );          
            
            bool stop = false;
            while( pthread_kill( _current_timer->_tid, 0 ) == 0 )
            {
                err = sigwait( &_block_mask, &signo );
                if( err != 0 ) {
                    exit( 1 );
                }
                if( signo == SIGALRM )
                {
                    LockGuard lock( &_mutex );
                    if( ! _current_timer->_play ) {
                        stop = true;
                    }                    
                    bool handled = _current_timer->_handled;
                    lock.unlock();
                    
                    if( handled ) 
                    {
                        _current_timer->_handled = 0;
                        break;                        
                    }
                    else                    
                        ++count;
                }
                else
                    exit(1);
            }
            if( ! _current_timer->_deleted )
            {
                _current_timer->_handling = false;
                if( ! stop ) 
                {
                    LockGuard lock( &_mutex );
                    _current_timer->_value = _current_timer->_interval;
                }
            }
            else
                delete _current_timer;               

            _current_timer = nullptr;
            stopAlrmSignal();
        }
        
        err = sigwait( &_block_mask, &signo );
        if( err != 0 ) {
            exit( 1 );
        }
        if( signo == SIGALRM )
        {
            LockGuard lock( &_mutex );              
            for( int i = 0; i < _number; ++i )
            {
                Timer* timer = _timers[ i ];
                if( ! timer->_play ) {
                    continue;
                }
                timer->_value -= MSEC_TIME_INTERVAL * ( count + 1 );
                
                if( timer->_value <= 0 ) 
                {
                    timer->_handling = true;
                    _queue->pushBack( timer );
                        
                    if( ! timer->_repeat ) 
                    {
                        timer->_play = false;
                        timer->_value = timer->_interval;
                        //remove( i );
                    }
                }
            }
            count = 0;
        }
        else
            exit(1);
    }
}

void* timersWorkFunc( void* arg )
{
    Timers* timers = (Timers*) arg;
    timers->handlingTimers();
    return (void*) 0;
}


void Timers::runHandler()
{
    _current_timer->runHandler();
    
    LockGuard lock( &_mutex );
    _current_timer->_handled = 1;
    lock.unlock();
       
    pthread_kill( _tid, SIGALRM );
}


void alrmFunc( int signo )
{
    Timers* timers = Timers::instance();
    timers->runHandler();
}


/*
void insideFunc( int signo )
{
    Timers* inst = Timers::instance();
    inst->_timer_alarm = 1;
    return;
}
*/


Timers::Timers()
    :   _timers(nullptr), 
        _number(0), 
        _timer_alarm(0),
        _current_timer(nullptr),
        _alrm_signal(false),
        _queue( nullptr )
{
    atexit( &Timers::cleanUp );

    _queue = new TimerQueue();
    
    _timers = new Timer* [ MAX_TIMERS_NUMBER ];
    
    pthread_mutex_init( &_mutex, NULL );
    
    sigset_t mask_vtalrm;
    struct sigaction act_vtalrm;
    
    sigemptyset( &mask_vtalrm );
    sigaddset( &mask_vtalrm, SIGVTALRM );
    
    memset( &act_vtalrm, 0, sizeof( act_vtalrm ) );
    act_vtalrm.sa_handler = alrmFunc;
    act_vtalrm.sa_mask = mask_vtalrm;
    sigaction( SIGVTALRM, &act_vtalrm, 0 );
    
    /*
    sigset_t mask_alrm;
    struct sigaction act_alrm;
    
    sigemptyset( &mask_alrm );
    sigaddset( &mask_alrm, SIGALRM );
    
    memset( &act_alrm, 0, sizeof( act_alrm ) );
    act_alrm.sa_handler = insideFunc;
    act_alrm.sa_mask = mask_alrm;
    sigaction( SIGALRM, &act_alrm, 0 );
    */

    sigemptyset( &_block_mask );
    sigaddset( &_block_mask, SIGALRM );
    
    if( pthread_sigmask( SIG_BLOCK, &_block_mask, NULL ) > 0 )
        exit(1);
    
    if( pthread_create( &_tid, NULL, &timersWorkFunc, this ) > 0 )
        exit(1);
}


Timers::~Timers()
{
    pthread_cancel( _tid );
    pthread_join( _tid, NULL );    
    
    pthread_mutex_destroy( &_mutex );
    
    delete[] _timers;
    _timers = nullptr;
    
    delete _queue;
    _queue = nullptr;
}


void Timers::cleanUp()
{
    delete _instance;
    _instance = nullptr;
}


Timers* Timers::instance()
{
    if( ! _instance )
        _instance = new Timers();
        
    return _instance;
}


Timer* Timers::addTimer( handler func, void* arg1, void* arg2 )
{
    LockGuard lock( &_mutex );
    if( _number < MAX_TIMERS_NUMBER )
    {
        Timer* timer = new Timer( this, func, arg1, arg2 );
        //startAlrmSignal();
        
        _timers[ _number ] = timer;
        ++_number;
        
        return timer;
    }
    else 
        return nullptr;
}


Timer* Timers::timer( int index ) const
{
    LockGuard lock( &_mutex );
    if( index >= 0 && index < _number )
        return _timers[ index ];
    else
        return nullptr;
}


void Timers::removeTimer( Timer* timer )
{
    LockGuard lock( &_mutex );
    if( timer != nullptr )
    {
        int num = find( timer->_id );
        remove( num );
    }
}


void Timers::removeAll()
{
    LockGuard lock( &_mutex );
    for( int i = 0; i < _number; ++i ) {
        delete _timers[ i ];
    }
    _number = 0;
    
    stopAlrmSignal();
}


int Timers::timersNumber() const
{
    LockGuard lock( &_mutex );
    return _number;
}


int Timers::find( int id )
{
    int index = -1;
    for( int i = 0; i < _number; ++i )
    {
        if( _timers[ i ]->_id == id )
        {
            index = i;
            break;
        }
    }
    return index;
}


void Timers::remove( int num )
{
    if( num >= 0 && num < _number )
    {
        Timer* timer = _timers[ num ];
        if( ! timer->_handling )
            delete timer;
        else
            timer->_deleted = true;
        
        for( int i = num; i < _number - 1; ++i )
        {
            _timers[ i ] = _timers[ i + 1 ];
            _timers[ i + 1 ] = nullptr;
        }
        --_number;
        
        stopAlrmSignal();
    }
}


void Timers::startAlrmSignal()
{
    if( ! _alrm_signal )
    {
        itimerval tval;
        
        timerclear(&tval.it_interval);
        timerclear(&tval.it_value);
        tval.it_value.tv_usec = MSEC_TIME_INTERVAL * 1000;
        tval.it_interval.tv_usec = MSEC_TIME_INTERVAL * 1000;
        
        setitimer( ITIMER_REAL, &tval, NULL );
        _alrm_signal = true;
    }
}


void Timers::stopAlrmSignal()
{
    if( _alrm_signal && _queue->size() == 0 )
    {
        bool stop = false;
        if( _number > 0 )
        {
            bool is_play = false;
            for( int i = 0; i < _number; ++i )
            {
                if( _timers[i]->_play ) 
                {
                    is_play = true;
                    break;
                }
            }
            if( ! is_play ) 
                stop = true;            
        }
        else
            stop = true;
        
        if( stop )
        {
            itimerval tval;
            
            timerclear(&tval.it_interval);
            timerclear(&tval.it_value);
            
            setitimer( ITIMER_REAL, &tval, NULL );
            _alrm_signal = false;            
        }
    }
}

