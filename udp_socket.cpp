

#include "udp_socket.h"
#include "functions.h"
#include "address.h"

#include <sys/select.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>


void system_error( const char* msg );


UdpSocket::UdpSocket()
    : _type( SocketType::block ), _is_bind(false), _is_connect( false ), _sockfd( 0 )
{
    if( ( _sockfd = socket( AF_INET, SOCK_DGRAM, 0 ) ) < 0 )
        exit( 1 );
}


UdpSocket::~UdpSocket()
{
    this->disconnect();
    close( _sockfd );
}


void UdpSocket::setType( SocketType type )
{   
    if( type == SocketType::nonblock ) 
        set_fl( _sockfd, O_NONBLOCK );
    else
        clr_fl( _sockfd, O_NONBLOCK );
    _type = type;
}


fd_set UdpSocket::getSet() const
{
    fd_set result_set;
    
    FD_ZERO( &result_set );
    FD_SET( _sockfd, &result_set );
    
    return result_set;
}


int UdpSocket::waitRecv( sigset_t* mask, int timeout_msec )
{
    int result = -1;
    if( _type == SocketType::nonblock )
    {
        fd_set set = getSet();
        timespec* timeout = NULL;
        if( timeout_msec >= 0 )
        {
            timeout = new timespec();
            timeout->tv_sec = timeout_msec / 1000; // seconds
            timeout->tv_nsec = timeout_msec % 1000 * 1000000; // nanoseconds
        }
        result = pselect( _sockfd + 1, &set, NULL, NULL, timeout, mask );
        if( timeout ) {
            delete timeout;
        }
    }
    return result;
}


int UdpSocket::waitSend( sigset_t* mask, int timeout_msec ) // milliseconds
{
    int result = -1;
    if( _type == SocketType::nonblock )
    {
        fd_set set = getSet();
        timespec* timeout = NULL;
        if( timeout_msec >= 0 )
        {    
            timeout = new timespec();
            timeout->tv_sec = timeout_msec / 1000; // seconds
            timeout->tv_nsec = timeout_msec % 1000 * 1000000 ; // nanoseconds
        }   
        result = pselect( _sockfd + 1, NULL, &set, NULL, timeout, mask );
        if( timeout ){
            delete timeout;
        }
    }
    return result;
}


bool UdpSocket::waitConnect( sigset_t* mask, int timeout_msec )
{
    return waitSend( mask, timeout_msec ) > 0;
}


bool UdpSocket::waitBind( sigset_t* mask, int timeout_msec )
{
    return waitSend( mask, timeout_msec ) > 0;
}


bool UdpSocket::connect( Address* addr )
{
    if( ! addr )
        return false;
    
    sockaddr_in sock_addr = (sockaddr_in) *addr;
    if( ::connect( _sockfd, ( sockaddr* ) &sock_addr, sizeof( sock_addr ) ) < 0 )
        return false;
    
    unsigned int len = sizeof( addr->_addr );
    getsockname ( _sockfd, ( sockaddr* ) &addr->_addr, &len );
    
    _is_connect = true;
    return true;
}


void UdpSocket::disconnect()
{
    if( _is_connect )
    {
        sockaddr_in addr;
        addr.sin_family = AF_UNSPEC;
        ::connect( _sockfd, ( sockaddr* ) &addr, sizeof( addr ) );

        _is_connect = false;
    }
}


bool UdpSocket::bind( Address* addr )
{
    if( ! addr )
        return false;
    
    sockaddr_in sock_addr = (sockaddr_in) *addr;
    if( ::bind( _sockfd, (sockaddr*) &sock_addr, sizeof( sock_addr ) ) < 0 )
        return false;
    
    unsigned int len = sizeof( addr->_addr );
    getsockname ( _sockfd, (sockaddr*) &addr->_addr, &len );
    
    _is_bind = true;
    return true;
}

void UdpSocket::unbind()
{
    if( _is_bind )
    {
        this->disconnect();
        close( _sockfd );
        
        if( ( _sockfd = socket( AF_INET, SOCK_DGRAM, 0 ) ) < 0 )
            exit( 1 );
        
        _is_bind = false;
    }
}


int UdpSocket::send( const char* data, int data_len, const Address* addr )
{
    int n = -1;
    if     ( _is_connect && ! addr ){
        n = ::send( _sockfd, data, data_len, 0 );
    }
    else if( ! _is_connect && addr )
    {
        sockaddr_in serv_addr = (sockaddr_in) *addr;
        n = sendto( _sockfd, data, data_len, 0, (sockaddr*) &serv_addr, sizeof( serv_addr ) );
    }
    return n;
}


int UdpSocket::receive( char* data, int data_len, Address* addr )
{
    int n = -1;
    if     ( _is_connect && ! addr )
    {
        if( ( n = ::recv( _sockfd, data, data_len, 0 ) ) >= 0 )
            data[ n ] = 0;
    }
    else if( ! _is_connect && addr )
    {
        sockaddr_in serv_addr;
        unsigned int len = sizeof(serv_addr);
        
        if( ( n = recvfrom( _sockfd, data, data_len, 0, (sockaddr*) &serv_addr, &len ) ) >= 0 )
        {
            data[ n ] = 0;
            *addr = serv_addr;
        }
    }
    return n;
}


bool UdpSocket::isConnect() const
{
    return _is_connect;
}