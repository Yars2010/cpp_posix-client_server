

#include "functions.h"

#include <sys/fcntl.h>
#include <signal.h>
#include <stdlib.h>

#ifdef DEBUG
    #include <stdio.h>
    #include <stdarg.h>
    #include <string.h>
#endif


#ifdef DEBUG
    extern FILE* debug_file;
#endif


bool sleepUs( int msec )
{
    timespec timeout;
    timeout.tv_sec = msec / 1000; // seconds
    timeout.tv_nsec = msec % 1000; // nanoseconds
    
    sigset_t mask;
    sigfillset( &mask );
    
    if( pselect( 0, NULL, NULL, NULL, &timeout, &mask ) < 0 )
        return false;
        
    return true;
}


void set_fl(int fd, int flags)
{
    int val;
    if ( (val = fcntl( fd, F_GETFL, 0) ) < 0 )
        exit( 1 );
    
    val |= flags;
    
    if ( fcntl( fd, F_SETFL, val ) < 0 )
        exit( 1 );
}


void clr_fl(int fd, int flags)
{
    int val;
    if ( (val = fcntl( fd, F_GETFL, 0) ) < 0 )
        exit( 1 );
    
    val &= ~flags;
    
    if ( fcntl( fd, F_SETFL, val ) < 0 )
        exit( 1 );
}

#ifdef DEBUG

void debug( const char* format, ... )
{
    if( debug_file )
    {
        char buffer[256];
        
        va_list args;
        va_start( args, format );
        
        vsprintf( buffer, format, args );
        //fwrite( buffer, sizeof(char), strlen(buffer), debug_file );
        fprintf( debug_file, "%s", buffer );
        fflush( debug_file );
        
        va_end( args );
    }
}

#endif