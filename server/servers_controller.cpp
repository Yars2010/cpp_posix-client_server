

#include "servers_controller.h"
#include "udp_server.h"
#include "ui.h"
#include "address.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>


ServersController::ServersController( Timers* timers )
    :   _ui(nullptr), 
        _udp_server(nullptr), 
        _address(nullptr), 
        _help(false), 
        _start(false), 
        _show(false), 
        _hide(false), 
        _info(false), 
        _clients(false), 
        _stop(false), 
        _quit(false)
{
    _udp_server = new UdpServer( timers );
}


ServersController::~ServersController()
{
    delete _udp_server;
}


void ServersController::setUiState( const char* format, ... )
{
    if( _ui ) 
    {
        va_list args;
        
        va_start( args, format );
        _ui->setState( true, true, format, args );
        va_end( args );
    } 
}


void ServersController::setUi( UserInterface* ui )
{
    if( ui ) 
    {
        _ui = ui;
        _udp_server->setUi( ui );
    }
    else
        exit( 1 );
}


void ServersController::run( const char* cmds )
{
    this->reset();
    this->parse( cmds );
    
    if( _start )
    {
        if( !_udp_server->isStart() )
            _udp_server->start( _address );
        else 
        {
            setUiState( "Server already run" );
            delete _address;
        } 
    }
    if( _show || _hide || _stop || _clients || _info )
    {
        if( _udp_server->isStart() )
        {
            if     ( _show )
                _udp_server->show();
            else if( _hide )
                _udp_server->hide();
            else if( _stop )
                _udp_server->stop();
            else if( _clients )
                _udp_server->showClientsInfo();
            else
                _udp_server->showTotalInfo();
        }
        else
            setUiState( "Server is not being run" );
    }
    else if( _quit )
        _udp_server->stop();
    else if( _help )
    {
        const char* text =
        
        "Help:\n\n"
        "start\ts\tstart work, listen address and port (xxx.xxx.xxx.xxx xxxxx)\n"
        "stop\t\tstop work\n"
        "info\t\tshow general information about server\n"
        "clients\t\tshow client's information\n"
        "show\t\tserver is visible\n"
        "hide\t\tserver is invisible\n"
        "quit\tq\tfinish the work\n"
        "help\th\tdisplay this help\n\n"
        "Press any key...";
        
        _ui->setText( text );
        _ui->switchText();
    }
}


bool ServersController::quit() const
{
    return _quit;
}


void ServersController::parse( const char* str )
{
    char prms[ INPUT_SIZE ];
    char args[ MAX_ARGS ][ INPUT_SIZE ];
    for( int i = 0; i < MAX_ARGS; ++i ){
        memset( args[i], 0, INPUT_SIZE );
    }
    
    strcpy( prms, str );
    
    char* pch;
    pch = strtok( prms, " ");
    
    int count = 0;
    for( ; pch != NULL && count < MAX_ARGS; ++count )
    {
        strcpy( args[count], pch );
        pch = strtok( NULL, " " );
    }
    
    if     ( ( strcmp( args[0], "help" ) == 0 || strcmp( args[0], "h" ) == 0 ) && count == 1 ) {
        _help = true;
    }
    else if( ( strcmp( args[0], "start" ) == 0 || strcmp( args[0], "s" ) == 0 ) ) 
    {
        if( strcmp( args[1], "localhost" ) == 0 && count == 2 )
        {
            _start = true;
            _address = new Address( SystemAddress::Loopback );
        }
        else if( count == 2 || count == 3 )
        {
            int a, b, c, d, port; char ch;
            
            int result = sscanf( args[1], "%3u.%3u.%3u.%3u%1c", &a, &b, &c, &d, &ch );
            if( ( result == 4 || ( result == 5 && ch == ' ' ) ) && a < 255 && b < 255 && c < 255 && d < 255 )
            {
                result = sscanf( args[2], "%5u%1c", &port, &ch );
                if( ( result == 1 || ( result == 2 && ch == ' ' ) ) && port < 65535 )
                {
                    _start = true;
                    _address = new Address( (unsigned char)a, (unsigned char)b, (unsigned char)c, (unsigned char)d, (unsigned short) port );                    
                }
                else
                    setUiState( "Error: invalid port\n" );
            }
            else
                setUiState( "Error: invalid ip\n" );
        }
        else
            setUiState( "Error: enter ip and port of server, example \"start xxx.xxx.xxx.xxx xxxxx\" or \"start localhost\"\n" );
    }
    else if( ( strcmp( args[0], "quit" ) == 0 || strcmp( args[0], "q" ) == 0 ) && count == 1 ) {
        _quit = true;
    }    
    else if( strcmp( args[0], "clients" ) == 0 && count == 1 ) {
        _clients = true;
    }
    else if( ( strcmp( args[0], "info" ) == 0 ) && count == 1 ) {
        _info = true;
    }
    else if( ( strcmp( args[0], "show" ) == 0 ) && count == 1 ) {
        _show = true;
    }
    else if( ( strcmp( args[0], "hide" ) == 0 ) && count == 1 ) {
        _hide = true;
    }
    else if( ( strcmp( args[0], "stop" ) == 0 ) && count == 1 ) {
        _stop = true;
    }
    else
        setUiState( "Error: invalid arguments, enter \"help\"\n" );
}


void ServersController::reset()
{
    _help = _start = _show = _hide = _info = _clients = _stop = _quit = false;
}