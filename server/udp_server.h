

#ifndef _UDP_SERVER_H_
#define _UDP_SERVER_H_


#include <pthread.h>
#include <signal.h>
#include "defines.h"


#define MAX_CLIENTS 256

class Timers;
class Timer;
class ClientsData;
class Address;
class UdpSocket;
class UserInterface;


void serverHandler( int signo );
void* workFunc( void* arg );
void stopAction( void* arg );
void timersHandler( void* arg1, void* arg2 );


enum Action{ Send, Receive };


class UdpServer
{
    UdpServer( const UdpServer* server );
    UdpServer& operator= ( const UdpServer* server );
    
public:
    UdpServer( Timers* timers );
    ~UdpServer();
    
    void setUi( UserInterface* ui );
    UserInterface* ui() const;
    void start( Address* address );
	void show();
    void hide();
    void stop();
    void setTimeout( int msec_timeout );
    int timeout() const;
    bool isStart() const;
    void showClientsInfo();
    int clientsNumber() const;
    void showTotalInfo();
    
private:
    friend void clientHandler( int signo );
    friend void* workFunc( void* arg );
    friend void timersHandler( void* arg1, void* arg2 );

    void initThread();
    void setUiState( bool priority, bool flash, const char* format, ... );
    bool binding( Address* address );
    bool talking();
    bool checkConnection();
    bool workWithActiveClinets( char* inbuff, int len, int clients_index );
    void workWithStartingClients( char* inbuff, int len, int clients_index );
    bool workWithNewClients( char* inbuff, int len, Address* address );
    bool restoreStream();
    int findActiveClient( Address* addr );
    int findStartingClient( Address* addr );
    int findClient( ClientsData** clients, int size, Address* addr );
    void clearTalkResource();
    void clearTimers();
    
    int send( Address* addr, const char* out_buff, int len, int timeout = -1 );
    int receive( Address* addr, char* in_buff, int len, int timeout = -1 );
    int waitReceiveAndSignal( Address* addr, char* in_buff, int in_len );
    
    UdpSocket* _socket;
    Address* _address;
    UserInterface* _ui;
    ClientsData **_clients, **_starting_clients;
    
    bool _user_start, _user_stop, _user_show, _user_hide, _user_clients_info, _user_total_info, _start, _visible, _clients_info;
    bool _sent_ok, _overflow_missing_packs;
    int _clients_num, _starting_clients_num, _attempts_number, _timeout;
    
    volatile sig_atomic_t _show_info, _check_link;
    Timers* _timers;
    Timer *_info_timer, *_connect_timer;
    int *_val1, *_val2;
    
    sigset_t _block_mask, _select_mask;
    pthread_t _tid;
    mutable pthread_mutex_t _lock;
    pthread_cond_t _ready;
};


#endif // _UDP_SERVER_H_