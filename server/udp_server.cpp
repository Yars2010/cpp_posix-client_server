

#include "udp_server.h"
#include "ui.h"
#include "clients_data.h"
#include "timers.h"
#include "lock_guard.h"
#include "udp_socket.h"
#include "functions.h"
#include "address.h"
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <stdio.h>


#define MAX_MISSING_PACKS 1024


class ClientsData
{
public:
    ClientsData( Address* address = nullptr );
    ~ClientsData();

    Address* _address;
    int *_packs, _number;
    unsigned long _total_bytes, _total_packs, _missing_packs, _restored_packs, _last_pack;
    bool _restore, _start_check, _connect_ok, _starting;
};


ClientsData::ClientsData( Address* address )
    :   _address( address ), 
        _packs(nullptr), 
        _number(0), 
        _total_bytes(0), 
        _total_packs(0),
        _missing_packs(0),
        _restored_packs(0), 
        _last_pack(0), 
        _restore(true),
        _start_check(false), 
        _connect_ok(true), 
        _starting(false)
{
    _packs = new int[ MAX_MISSING_PACKS ];
}

ClientsData::~ClientsData()
{
    delete[] _packs;
}


UdpServer::UdpServer( Timers* timers )
    :   _socket(nullptr), 
        _address(nullptr), 
        _ui(nullptr),
        _clients(nullptr),
        _starting_clients(nullptr),
        _user_start(false), 
        _user_stop(false), 
        _user_show(false), 
        _user_hide(false), 
        _user_clients_info(false), 
        _user_total_info(false),
        _start(false), 
        _visible(true), 
        _clients_info(false),
        _sent_ok(true),
        _overflow_missing_packs(false),
        _clients_num(0), 
        _timeout( TIMEOUT ), 
        _show_info(0), 
        _check_link(0),
        _timers( timers ),
        _info_timer(nullptr), 
        _connect_timer(nullptr), 
        _val1(new int(1)),
        _val2(new int(2))
{
    pthread_mutex_init( &_lock, NULL );
    pthread_cond_init( &_ready, NULL );
    
    _clients = new ClientsData* [ MAX_CLIENTS ];
    _starting_clients = new ClientsData* [ MAX_CLIENTS ];
    
    sigemptyset( &_block_mask );
    sigaddset( &_block_mask, SIGUSR2 );
    sigaddset( &_block_mask, SIGVTALRM );
    
    sigfillset( &_select_mask );
    sigdelset( &_select_mask, SIGUSR2 );
    sigdelset( &_select_mask, SIGVTALRM );
    
    _socket = new UdpSocket();
    _socket->setType( SocketType::nonblock );

    if( pthread_create( &_tid, NULL, &workFunc, this ) > 0 )
        exit( 1 );
}


UdpServer::~UdpServer()
{
    pthread_cancel( _tid );
    pthread_join( _tid, NULL );    
    
    clearTalkResource();
    clearTimers();
    
    delete _socket;
    _socket = nullptr;    
    
    delete[] _starting_clients;
    delete[] _clients;
    
    pthread_mutex_destroy( &_lock );
    pthread_cond_destroy( &_ready );
    
    delete _val1;
    delete _val2;
    _val1 = _val2 = nullptr;
}


void UdpServer::setUi( UserInterface* ui )
{
    if( ! ui )
        exit( 1 );
        
    _ui = ui;
}


UserInterface* UdpServer::ui() const
{
    return _ui;
}


void UdpServer::start( Address* address )
{
    LockGuard lock( &_lock );
    if( ! _start )
    {
        _address = address;
        _user_start = true;
        pthread_kill( _tid, SIGUSR2 );
    }
}


void UdpServer::show()
{
    LockGuard lock( &_lock );
    if( _start )
    {
        _user_show = true;
        pthread_kill( _tid, SIGUSR2 );
    }
}


void UdpServer::hide()
{
    LockGuard lock( &_lock );
    if( _start )
    {
        _user_hide = true;
        pthread_kill( _tid, SIGUSR2 );
    }
}


void UdpServer::showClientsInfo()
{
    LockGuard lock( &_lock );
    if( _start )
    {
        _user_clients_info = true;
        pthread_kill( _tid, SIGUSR2 );
    }
}


void UdpServer::showTotalInfo()
{
    LockGuard lock( &_lock );
    if( _start )
    {
        _user_total_info = true;
        pthread_kill( _tid, SIGUSR2 );
    }
}


int UdpServer::clientsNumber() const 
{ 
    LockGuard lock( &_lock );
    return _clients_num;
}


void UdpServer::stop()
{
    LockGuard lock( &_lock );
    if( _start )
    {
        _user_stop = true;
        pthread_kill( _tid, SIGUSR2 );
    }
}


void UdpServer::setUiState( bool priority, bool flash, const char* format, ... )
{
    if( _ui ) 
    {
        va_list args;
        
        va_start( args, format );
        _ui->setState( priority, flash, format, args );
        va_end( args );
    }
}


void UdpServer::setTimeout( int msec_timeout )
{
    LockGuard lock( &_lock );
    if( msec_timeout >= MIN_TIMEOUT && msec_timeout <= MAX_TIMEOUT )
        _timeout = msec_timeout;
    else if( msec_timeout > MAX_TIMEOUT )
        _timeout = MAX_TIMEOUT;
    else
        _timeout = MIN_TIMEOUT;
}


int UdpServer::timeout() const
{
    LockGuard lock( &_lock );
    return _timeout;
}


bool UdpServer::isStart() const
{
    LockGuard lock( &_lock );
    return _start;
}


void serverHandler( int signo )
{
    if( signo == SIGUSR2 )
        return;
    else
        exit( 1 );
}


void timersHandler( void* arg1, void* arg2 )
{
    UdpServer* server = (UdpServer*) arg1; // current time don't call "new"
    int* val = (int*) arg2;
    if     ( *val == 1 )
        server->_show_info = 1;
    else if( *val == 2 )
        server->_check_link = 1;
    else
        exit( 1 );
}


int UdpServer::findClient( ClientsData** clients, int size, Address* addr )
{
    int result = -1;
    for( int i = 0; i < size; ++i )
    {
        if( *( clients[ i ]->_address ) == *addr )
        {
            result = i;
            break;
        }
    }
    return result;    
}


int UdpServer::findActiveClient( Address* addr )
{
    return findClient( _clients, _clients_num, addr );
}


int UdpServer::findStartingClient( Address* addr )
{
    return findClient( _starting_clients, _starting_clients_num, addr );   
}


int UdpServer::send( Address* addr, const char* out_buff, int len, int timeout )
{
    int data_size = _socket->send( out_buff, len, addr );
    int result = _socket->waitSend( &_select_mask, timeout );
    if( result <= 0 )
        return result;
    
    return data_size;
}


int UdpServer::receive( Address* addr, char* in_buff, int len, int timeout )
{    
    int result = _socket->waitRecv( &_select_mask, timeout );
    if( result <= 0 ) {
        return result;
    }
    int data_size = _socket->receive( in_buff, len, addr );
    return data_size;
}


bool UdpServer::checkConnection()
{
    sigset_t old_mask;
    pthread_sigmask( SIG_UNBLOCK, &_block_mask, &old_mask );
    
    for( int i = 0; i < _clients_num; ) 
    {
        ClientsData* client = _clients[ i ];
        if( _clients[ i ]->_start_check )
        {
            delete client;
            for( int j = i; j < _clients_num - 1; ++j ) {
                _clients[ j ] = _clients[ j + 1 ];
            }
            --_clients_num;
            
            continue;
        }
        _sent_ok = send( _clients[ i ]->_address, "clntavail", 10 ) > 0;
        if( _sent_ok ) 
        {
            _clients[ i ]->_start_check = true;
            ++i;            
        }
        else
            break;
    }
    if( _sent_ok )
    {
        for( int i = 0; i < _starting_clients_num; ++i ) {
            delete _starting_clients[ i ];
        }
        _starting_clients_num = 0;
    }
    if( _clients_num == 0 && _starting_clients_num == 0 ) {
        _connect_timer->stop();
    }
    
    pthread_sigmask( SIG_SETMASK, &old_mask, NULL );       
    return _sent_ok;
}




bool UdpServer::workWithActiveClinets( char* inbuff, int len, int clients_index )
{
    ClientsData* client = _clients[ clients_index ];
    
    long pack;
    int result = sscanf( inbuff, "%ld", &pack );
    if( result == 1 )
    {
        int diff = pack - client->_last_pack;
        if( diff > 0 )
        {
            if     ( diff == 1 ) {
                client->_last_pack = pack;
            }
            else if( diff > 1 )
            {
                int missing = diff - 1;
                client->_missing_packs += missing;
                
                if( client->_missing_packs < MAX_MISSING_PACKS )
                {
                    for( int i = missing; i > 0; --i ){
                        client->_packs[ client->_number++ ] = pack - i;
                    }
                    client->_last_pack = pack;
                }
                else
                {
                    _overflow_missing_packs = true;
                    return false;
                }
            }
            client->_total_bytes += len;
            ++client->_total_packs;
        }
        else if( diff < 0 && client->_restore )
        {
            bool found = false;
            
            int index = 0;
            for( ; index < client->_number; ++index )
            {
                if( client->_packs[ index ] == pack )
                {
                    found = true;
                    break;
                }
            }
            if( found )
            {
                for( int i = index; i < client->_number - 1; ++i ) {
                    client->_packs[ i ] = client->_packs[ i + 1 ];
                }
                --client->_number;
                ++client->_restored_packs;
                ++client->_total_packs;
                
                client->_total_bytes += len;
            }
        }       
    }
    else if( len == 10 && memcmp( inbuff, "servavail", 9 ) == 0 )
    {
        _sent_ok = send( client->_address, "servavail", 10 ) > 0;
        if( ! _sent_ok ) {
            return false;
        }
    }
    else if( len == 10 && memcmp( inbuff, "clntavail", 9 ) == 0 )
    {
        client->_start_check = false;
        client->_connect_ok = true;
    }

    return true;
}


bool UdpServer::workWithNewClients( char* inbuff, int len, Address* address )
{
    if( _visible && len == 6 && memcmp( inbuff, "start", 5 ) == 0 )
    {
        if( _starting_clients_num <= MAX_CLIENTS && _clients_num <= MAX_CLIENTS ) 
        {
            Address* new_address = new Address( *address );
            _starting_clients[ _starting_clients_num++ ] = new ClientsData( new_address );
            
            _sent_ok = send( new_address, "startok", 8 ) > 0;
            if( ! _sent_ok ) {
                return false;
            }
            if( _connect_timer->isStop() ) {
                _connect_timer->play( timeout(), true);
            }
        }
        else 
            setUiState( true, true, "The connection is rejected. Max is %d clients", MAX_CLIENTS );
    }
    return true;    
}


void UdpServer::workWithStartingClients( char* inbuff, int len, int clients_index )
{
    ClientsData* client = _starting_clients[ clients_index ];
    if( len == 8 && memcmp( inbuff, "startok", 7 ) == 0 )
    {
        _clients[ _clients_num++ ] = client;
        
        for( int i = clients_index; i < _starting_clients_num - 1; ++i ) {
            _starting_clients[ i ] = _starting_clients[ i + 1 ];
        }
        --_starting_clients_num;                
    }
}


bool UdpServer::restoreStream()
{
    for( int i = 0; i < _clients_num; ++i )
    {
        ClientsData* client = _clients[ i ];
        if( client->_restore && client->_number > 0 )
        {
            int buff_size = 32 * MAX_MISSING_PACKS;
            char outbuff[ buff_size ];
            
            sprintf( outbuff, "repeat" );
            for( int j = 0; j < client->_number; ++j )
            {
                char str[32];
                str[0] = ' ';
                sprintf( str + 1, "%d", client->_packs[j] );
                strcat( outbuff, str );
            }
            
            _sent_ok = send( client->_address, outbuff, strlen( outbuff ) ) > 0;
            if( ! _sent_ok ) {
                return false;
            }
        }
    }
    return true;
}


void UdpServer::clearTimers()
{
    _timers->removeTimer( _info_timer );
    _timers->removeTimer( _connect_timer );
    
    _info_timer = _connect_timer = nullptr;
}


void UdpServer::clearTalkResource()
{
    _info_timer->stop();
    _connect_timer->stop();
    
    for( int i = 0; i < _starting_clients_num; ++i ){
        delete _starting_clients[ i ];
    }
    _starting_clients_num = 0;
    
    for( int i = 0; i < _clients_num; ++i ) {
        delete _clients[ i ];
    }
    _clients_num = 0;
    _start = false;
    _check_link = _show_info = 0;
    
    if( _address ) 
    {
        delete _address;
        _address = nullptr;
    }
    _socket->unbind();
}


bool UdpServer::binding( Address* address )
{
    sigset_t mask;
    pthread_sigmask( 0, NULL, &mask );
    
    _socket->bind( address );
    return _socket->waitBind( &mask, _timeout );
}


int UdpServer::waitReceiveAndSignal( Address* addr, char* in_buff, int in_len )
{
    sigset_t old_mask;
    pthread_sigmask( SIG_BLOCK, &_block_mask, &old_mask );    
    
    int result = -1;
    while( true )
    {
        LockGuard lock( &_lock );
        if     ( _user_start && ! _start )
        {
            _user_start = false;
            if( ! this->binding( _address ) )
            {
                setUiState( true, true, "Error: can not binding address" );
                _socket->unbind();
            }
            
            _start = true;
            _info_timer->play( 1000, true );
            setUiState( true, true, "Starting..." );
        }
        else if( _user_stop && _start )
        {
            _user_stop = false;
            _info_timer->stop();
            clearTalkResource();
            setUiState( true, true, "Stopped" );
        }
        else if( _user_show )
        {
            _user_show = false;
            _visible = true;
            setUiState( true, true, "Server is visible" );
        }
        else if( _user_hide )
        {
            _user_hide = false;
            _visible = false;
            setUiState( true, true, "Server is not visible" );
        }
        else if( _user_clients_info )
        {
            _user_clients_info = false;
            _clients_info = true;
            setUiState( true, true, "Show clients info" );
        }        
        else if( _user_total_info )
        {
            _user_total_info = false;
            _clients_info = false;
            setUiState( true, true, "Show server info" );
        }
        else if( _check_link )
        {
            _check_link = 0;
            if( ! checkConnection() || ! restoreStream() ) {
                break;
            }
        }           
        else if( _show_info )
        {
            _show_info = 0;
            if( _clients_info )
            {
                char total_state[ 128 * MAX_CLIENTS ];
                total_state[0] = '\0';
                
                for( int i = 0; i < _clients_num; ++i )
                {
                    ClientsData* client = _clients[ i ];
                    
                    char client_state[ 128 ];
                    sprintf( client_state, "Client %d: bytes: %lu, packs: %lu, missing packs: %lu, restored packs: %lu\n", 
                                            i + 1,
                                            client->_total_bytes, 
                                            client->_total_packs, 
                                            client->_missing_packs, 
                                            client->_restored_packs );
                                            
                    strcat( total_state, client_state );
                }
                
                if( _clients_num == 0 )
                    sprintf( total_state, "Sorry! No clients" );
                
                setUiState( false, true, total_state );
            }
            else
                setUiState( false, true, "ip: %s, connected clients: %d", _address->str(), _clients_num );
        }
        lock.unlock();
        
        result = receive( addr, in_buff, in_len );
        if( result >= 0 )
            break;
    }
    
    pthread_sigmask( SIG_SETMASK, &old_mask, NULL );    
    return result;
}


void UdpServer::initThread()
{
    sigset_t mask;
    sigemptyset( &mask );
    sigaddset( &mask, SIGUSR2 );
    
    struct sigaction act;
    memset( &act, 0, sizeof(act) );
    act.sa_handler = serverHandler;
    act.sa_mask = mask;
    sigaction( SIGUSR2, &act, 0 );
    
    _info_timer = _timers->addTimer( timersHandler, (void*)this, (void*)_val1 );
    _connect_timer = _timers->addTimer( timersHandler, (void*)this, (void*)_val2 );    
}


bool UdpServer::talking()
{
    this->initThread();
    
    char inbuff [ MAX_DATA_SIZE ];
    
    while( true )
    {
        Address address;
        int len = 0, clients_index = -1;
        while( ( len = waitReceiveAndSignal( &address, inbuff, MAX_DATA_SIZE ) ) > 0 ) // infinite waiting
        {
            clients_index = findActiveClient( &address );
            if( clients_index >= 0 )
            {
                bool work_result = workWithActiveClinets( inbuff, len, clients_index );
                if( ! work_result ){
                    break;
                }
            }
            else
            {
                clients_index = findStartingClient( &address );
                if( clients_index >= 0 ) {
                    workWithStartingClients( inbuff, len, clients_index );
                }
                else 
                {
                    bool work_result = workWithNewClients( inbuff, len, &address );
                    if( ! work_result ) {
                        break;
                    }
                }
            }
        }
        if( _overflow_missing_packs )
        {
            delete _clients[ clients_index ];
            for( int j = clients_index; j < _clients_num - 1; ++j ) {
                _clients[ j ] = _clients[ j + 1 ];
            }
            --_clients_num;
            
            _overflow_missing_packs = false;
        }
        if( ! _sent_ok )
        {
            clearTalkResource();
            setUiState( true, false, "Error: network is not available" );
        }
    }
    return true;
}


void* workFunc( void* arg )
{
    UdpServer* server = (UdpServer*) arg;
    server->talking();
    return (void*) 0;
}
