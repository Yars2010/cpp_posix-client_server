

#include "ui.h"
#include "timers.h"
#include "servers_controller.h"


#ifdef DEBUG
    #include <stdio.h>
    #include <stdlib.h>
#endif

#ifdef DEBUG
    FILE* debug_file;
#endif


int main( int argc, char** argv )
{
    #ifdef DEBUG
        debug_file = fopen( "debug.txt", "w" );
    #endif
    
    Timers* timers = Timers::instance();
	ServersController* controller = new ServersController( timers );
    
    UserInterface ui( controller, timers );
    ui.setName( "Server ver 1.0" );
    ui.start();

    delete controller;

    #ifdef DEBUG
        fclose( debug_file );
    #endif

	return 0;
}
