##
## Auto Generated makefile by CodeLite IDE
## any manual changes will be erased      
##
## Release
ProjectName            :=server
ConfigurationName      :=Release
WorkspacePath          :=/home/silver-boy/Source/cpp/cpp_posix-client_server
ProjectPath            :=/home/silver-boy/Source/cpp/cpp_posix-client_server/server
IntermediateDirectory  :=./release
OutDir                 := $(IntermediateDirectory)
CurrentFileName        :=
CurrentFilePath        :=
CurrentFileFullPath    :=
User                   :=silverboy
Date                   :=11/21/19
CodeLitePath           :=/home/silver-boy/.codelite
LinkerName             :=g++
SharedObjectLinkerName :=g++ -shared -fPIC
ObjectSuffix           :=.o
DependSuffix           :=.o.d
PreprocessSuffix       :=.o.i
DebugSwitch            :=-gstab
IncludeSwitch          :=-I
LibrarySwitch          :=-l
OutputSwitch           :=-o 
LibraryPathSwitch      :=-L
PreprocessorSwitch     :=-D
SourceSwitch           :=-c 
OutputFile             :=$(IntermediateDirectory)/$(ProjectName)
Preprocessors          :=$(PreprocessorSwitch)NDEBUG 
ObjectSwitch           :=-o 
ArchiveOutputSwitch    := 
PreprocessOnlySwitch   :=-E 
ObjectsFileList        :="server.txt"
PCHCompileFlags        :=
MakeDirCommand         :=mkdir -p
LinkOptions            :=  
IncludePath            :=  $(IncludeSwitch). $(IncludeSwitch).. 
IncludePCH             := 
RcIncludePath          := 
Libs                   := $(LibrarySwitch)pthread $(LibrarySwitch)ncurses 
ArLibs                 :=  "pthread" "ncurses" 
LibPath                := $(LibraryPathSwitch). 

##
## Common variables
## AR, CXX, CC, AS, CXXFLAGS and CFLAGS can be overriden using an environment variables
##
AR       := ar rcus
CXX      := g++
CC       := gcc
CXXFLAGS :=  -O2 -Wall $(Preprocessors)
CFLAGS   :=  -O2 -Wall $(Preprocessors)
ASFLAGS  := 
AS       := as


##
## User defined environment variables
##
CodeLiteDir:=/usr/share/codelite
Objects0=$(IntermediateDirectory)/servers_controller.cpp$(ObjectSuffix) $(IntermediateDirectory)/udp_server.cpp$(ObjectSuffix) $(IntermediateDirectory)/up_lock_guard.cpp$(ObjectSuffix) $(IntermediateDirectory)/up_udp_socket.cpp$(ObjectSuffix) $(IntermediateDirectory)/up_ui.cpp$(ObjectSuffix) $(IntermediateDirectory)/main.cpp$(ObjectSuffix) $(IntermediateDirectory)/up_timers.cpp$(ObjectSuffix) $(IntermediateDirectory)/up_address.cpp$(ObjectSuffix) $(IntermediateDirectory)/up_functions.cpp$(ObjectSuffix) 



Objects=$(Objects0) 

##
## Main Build Targets 
##
.PHONY: all clean PreBuild PrePreBuild PostBuild MakeIntermediateDirs
all: $(OutputFile)

$(OutputFile): $(IntermediateDirectory)/.d $(Objects) 
	@$(MakeDirCommand) $(@D)
	@echo "" > $(IntermediateDirectory)/.d
	@echo $(Objects0)  > $(ObjectsFileList)
	$(LinkerName) $(OutputSwitch)$(OutputFile) @$(ObjectsFileList) $(LibPath) $(Libs) $(LinkOptions)

MakeIntermediateDirs:
	@test -d ./release || $(MakeDirCommand) ./release


$(IntermediateDirectory)/.d:
	@test -d ./release || $(MakeDirCommand) ./release

PreBuild:


##
## Objects
##
$(IntermediateDirectory)/servers_controller.cpp$(ObjectSuffix): servers_controller.cpp $(IntermediateDirectory)/servers_controller.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/home/silver-boy/Source/cpp/cpp_posix-client_server/server/servers_controller.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/servers_controller.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/servers_controller.cpp$(DependSuffix): servers_controller.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/servers_controller.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/servers_controller.cpp$(DependSuffix) -MM servers_controller.cpp

$(IntermediateDirectory)/servers_controller.cpp$(PreprocessSuffix): servers_controller.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/servers_controller.cpp$(PreprocessSuffix) servers_controller.cpp

$(IntermediateDirectory)/udp_server.cpp$(ObjectSuffix): udp_server.cpp $(IntermediateDirectory)/udp_server.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/home/silver-boy/Source/cpp/cpp_posix-client_server/server/udp_server.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/udp_server.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/udp_server.cpp$(DependSuffix): udp_server.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/udp_server.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/udp_server.cpp$(DependSuffix) -MM udp_server.cpp

$(IntermediateDirectory)/udp_server.cpp$(PreprocessSuffix): udp_server.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/udp_server.cpp$(PreprocessSuffix) udp_server.cpp

$(IntermediateDirectory)/up_lock_guard.cpp$(ObjectSuffix): ../lock_guard.cpp $(IntermediateDirectory)/up_lock_guard.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/home/silver-boy/Source/cpp/cpp_posix-client_server/lock_guard.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/up_lock_guard.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/up_lock_guard.cpp$(DependSuffix): ../lock_guard.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/up_lock_guard.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/up_lock_guard.cpp$(DependSuffix) -MM ../lock_guard.cpp

$(IntermediateDirectory)/up_lock_guard.cpp$(PreprocessSuffix): ../lock_guard.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/up_lock_guard.cpp$(PreprocessSuffix) ../lock_guard.cpp

$(IntermediateDirectory)/up_udp_socket.cpp$(ObjectSuffix): ../udp_socket.cpp $(IntermediateDirectory)/up_udp_socket.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/home/silver-boy/Source/cpp/cpp_posix-client_server/udp_socket.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/up_udp_socket.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/up_udp_socket.cpp$(DependSuffix): ../udp_socket.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/up_udp_socket.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/up_udp_socket.cpp$(DependSuffix) -MM ../udp_socket.cpp

$(IntermediateDirectory)/up_udp_socket.cpp$(PreprocessSuffix): ../udp_socket.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/up_udp_socket.cpp$(PreprocessSuffix) ../udp_socket.cpp

$(IntermediateDirectory)/up_ui.cpp$(ObjectSuffix): ../ui.cpp $(IntermediateDirectory)/up_ui.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/home/silver-boy/Source/cpp/cpp_posix-client_server/ui.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/up_ui.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/up_ui.cpp$(DependSuffix): ../ui.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/up_ui.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/up_ui.cpp$(DependSuffix) -MM ../ui.cpp

$(IntermediateDirectory)/up_ui.cpp$(PreprocessSuffix): ../ui.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/up_ui.cpp$(PreprocessSuffix) ../ui.cpp

$(IntermediateDirectory)/main.cpp$(ObjectSuffix): main.cpp $(IntermediateDirectory)/main.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/home/silver-boy/Source/cpp/cpp_posix-client_server/server/main.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/main.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/main.cpp$(DependSuffix): main.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/main.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/main.cpp$(DependSuffix) -MM main.cpp

$(IntermediateDirectory)/main.cpp$(PreprocessSuffix): main.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/main.cpp$(PreprocessSuffix) main.cpp

$(IntermediateDirectory)/up_timers.cpp$(ObjectSuffix): ../timers.cpp $(IntermediateDirectory)/up_timers.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/home/silver-boy/Source/cpp/cpp_posix-client_server/timers.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/up_timers.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/up_timers.cpp$(DependSuffix): ../timers.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/up_timers.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/up_timers.cpp$(DependSuffix) -MM ../timers.cpp

$(IntermediateDirectory)/up_timers.cpp$(PreprocessSuffix): ../timers.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/up_timers.cpp$(PreprocessSuffix) ../timers.cpp

$(IntermediateDirectory)/up_address.cpp$(ObjectSuffix): ../address.cpp $(IntermediateDirectory)/up_address.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/home/silver-boy/Source/cpp/cpp_posix-client_server/address.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/up_address.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/up_address.cpp$(DependSuffix): ../address.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/up_address.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/up_address.cpp$(DependSuffix) -MM ../address.cpp

$(IntermediateDirectory)/up_address.cpp$(PreprocessSuffix): ../address.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/up_address.cpp$(PreprocessSuffix) ../address.cpp

$(IntermediateDirectory)/up_functions.cpp$(ObjectSuffix): ../functions.cpp $(IntermediateDirectory)/up_functions.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/home/silver-boy/Source/cpp/cpp_posix-client_server/functions.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/up_functions.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/up_functions.cpp$(DependSuffix): ../functions.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/up_functions.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/up_functions.cpp$(DependSuffix) -MM ../functions.cpp

$(IntermediateDirectory)/up_functions.cpp$(PreprocessSuffix): ../functions.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/up_functions.cpp$(PreprocessSuffix) ../functions.cpp


-include $(IntermediateDirectory)/*$(DependSuffix)
##
## Clean
##
clean:
	$(RM) -r ./release/


