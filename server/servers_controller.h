

#ifndef _SERVERS_CONTROLLER_H_
#define _SERVERS_CONTROLLER_H_


#include "controller.h"


#define MAX_ARGS 3


class UserInterface;
class UdpServer;
class Address;
class Timers;


class ServersController: public Controller
{
public:
    ServersController( Timers* timers );
    ~ServersController();

    void setUi( UserInterface* ui ) override;
    void run( const char* cmds ) override;
    bool quit() const override;
    
private:
    void setUiState( const char* format, ... );
    void parse( const char* str );
    void reset();
    
    UserInterface* _ui;
    UdpServer* _udp_server;
    Address* _address;
    bool _help, _start, _show, _hide, _info, _clients, _stop, _quit;
};


#endif // _SERVERS_CONTROLLER_H_