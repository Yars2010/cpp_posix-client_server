

#ifndef _LOCK_GUARD_H_
#define _LOCK_GUARD_H_


#include <pthread.h>


enum GuardType{ None, Lock };


class LockGuard
{
public:
    LockGuard( pthread_mutex_t* mutex, GuardType = GuardType::Lock );
    ~LockGuard();
    
    bool tryLock();
    void lock();
    void unlock();
    bool isLock();
    
private:
    LockGuard( const LockGuard& lock_guard );
    LockGuard& operator=( const LockGuard& lock_guard );
    
    pthread_mutex_t* _mutex;
    GuardType _type;
    bool _lock;
};


#endif // _LOCK_GUARD_H_
