

#ifndef _ADDRESS_H_
#define _ADDRESS_H_


#include <sys/types.h>
#include <netinet/in.h>


typedef unsigned char  u_char;
typedef unsigned short u_short;


const unsigned int ADDRLEN = INET_ADDRSTRLEN + 7;


enum SystemAddress 
{ 
    Loopback, 
    Any
};


class Address
{
    friend class UdpSocket;
    
public:
    Address();
    Address( SystemAddress type );
	Address( u_char a, u_char b, u_char c, u_char d, u_short port );
    ~Address();
	
    Address( const Address& address ) = default;
    Address( sockaddr_in addr );
    
    Address& operator=( const Address& address ) = default;
    Address& operator=( sockaddr_in addr );
    
    const char* str();

	bool operator ==( const Address& address ) const;    
	operator sockaddr_in () const;
    
private:
    void readAddr();
    void addressToSockAddr( u_char a, u_char b, u_char c, u_char d, u_short port, sockaddr_in* addr ) const;

    sockaddr_in _addr;
    char* _str_addr;
};

#endif // _ADDRESS_H_