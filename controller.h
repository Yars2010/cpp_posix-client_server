

#ifndef _CONTROLLER_H_
#define _CONTROLLER_H_


class UserInterface;


class Controller
{
public:
    virtual ~Controller() { }

    virtual void setUi( UserInterface* ui ) = 0;
    virtual void run( const char* cmds ) = 0;
    virtual bool quit() const = 0;
};


#endif // _CONTROLLER_H_