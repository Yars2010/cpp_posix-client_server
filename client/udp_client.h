

#ifndef _UDP_CLIENT_H_
#define _UDP_CLIENT_H_


#include <pthread.h>
#include <signal.h>
#include "defines.h"


class Address;
class UdpSocket;
class UserInterface;
class Timer;
class Timers;
class IntQueue; 


void* clientWorkFunc( void* arg );
void timersHandler( void* arg1, void* arg2 );
void clientHandler( int signo );


enum Action{ Send, Receive };


class UdpClient
{
	UdpClient( const UdpClient& client );
	UdpClient& operator=( const UdpClient& client );
    
public:
    UdpClient( Timers* timers );
	~UdpClient();

    void setUi( UserInterface* ui );
    UserInterface* ui() const;
	bool start( Address* address = nullptr );
    bool isStart() const;
    void pause();
    bool isPause() const;
    void stop();
    void skip( int packs_number );
    void setTimeout( int timeout );
    int timeout() const;
	void setSpeed( int speed );
    int speed() const;
    
private:
    friend void* clientWorkFunc( void* arg );
    friend void timersHandler( void* arg1, void* arg2 );
    friend void clientHandler( int signo );
    
    bool socketConnect();
    bool talking();
    
    bool incDataSize( int step );
    bool decInterval( int step );
    bool decDataSize( int step );
    bool incInterval( int step );
    void correctSpeedParams();
    void calcSizeAndInterval();
    
    int waitReceiveAndSignal( char* in_buff, int len );
    int send( const char* out_buff, int len, int timeout = -1 );
    int receive( char* in_buff, int len, int timeout = -1 );
    void clearTalkResource();
    void clearTimers();
    
    void calcSpeed();
    void initThread();
    void setUiState( bool priority, bool flash, const char* format, ... );
    
	UdpSocket* _socket;
    Address* _address;
    UserInterface* _ui;
    
    pthread_t _tid1;
    mutable pthread_mutex_t _lock;
    pthread_cond_t _ready;
    sigset_t _block_mask, _select_mask; 
    
    bool _user_start, _user_stop, _user_play, _user_pause, _user_skip, _user_speed, _user_timeout, _start, _pause;
    bool _begin_check_link, _link_ok, _begin_check_connect, _connect_ok, _sent_ok;
    volatile sig_atomic_t _check_link, _check_speed, _check_connect;    
    
    int _timeout, _speed, _skip_number, _data_size, _send_interval, _total_bytes, _size_step; 
    double _curr_speed, _last_diff;
    bool _correct_interval, _curr_speed_more, _curr_seed_less;
    long _pack;
    
    IntQueue* _repeat_packs;      
    Timers* _timers;
    Timer *_speed_timer, *_connect_timer, *_link_timer;
    int *_val1, *_val2, *_val3, *_val4;
    
    timespec _lastcall_time;
};


#endif // _UDP_CLIENT_H_