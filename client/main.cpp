

#include "ui.h"
#include "timers.h"
#include "clients_controller.h"

#ifdef DEBUG
    #include <stdio.h>
    #include <stdlib.h>
#endif

#ifdef DEBUG
    FILE* debug_file;
#endif


int main( int argc, char **argv )
{
    #ifdef DEBUG
        debug_file = fopen( "debug.txt", "w" );
    #endif
    
    Timers* timers = Timers::instance();
    ClientsController* controller = new ClientsController( timers );
    
    UserInterface ui( controller, timers );
    ui.setName( "Client ver. 1.0" );
    ui.start();
    
    delete controller;
    
    #ifdef DEBUG
        fclose( debug_file );
    #endif
    
	return 0;
}
