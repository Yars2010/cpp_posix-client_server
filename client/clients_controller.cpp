

#include "clients_controller.h"
#include "ui.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>



ClientsController::ClientsController( Timers* timers )
    : _ui( nullptr ), _udp_client( nullptr ), _address( nullptr ), _help( false ), _quit( false ), _start( false ), _pause( false ), 
        _stop( false ), _set_speed( false ), _skip( false ), _set_timeout(false), _speed( 0 ), _skip_number( 0 ), _timeout( 0 )
{   
    _udp_client = new UdpClient( timers );
}


ClientsController::~ClientsController()
{
    delete _udp_client;
}

void ClientsController::setUi( UserInterface* ui )
{
    if( ui )
    {
        _ui = ui;
        _udp_client->setUi( ui );        
    }
    else
        exit( 1 );
}

void ClientsController::run( const char* cmds )
{
    this->parse( cmds );
    if( _start ) 
    {
        if     ( _address )
        {
            if( ! _udp_client->isStart() )
                _udp_client->start( _address );
            else
                setUiState( "Client already run" );
        }
        else if( _udp_client->isPause() )
            _udp_client->start();
        else
            setUiState( "Enter ip and port of server, example \"start xxx.xxx.xxx.xxx xxxxx\"\n" );
    }
    else if( _stop || _pause || _skip )
    {
        if( _udp_client->isStart() )
        {
            if( _stop  )
                _udp_client->stop();
            else if( _pause )
                _udp_client->pause();
            else if( _skip )
                _udp_client->skip( _skip_number );
        }
        else
            setUiState( "Client is not being run" );
    }
    else if( _set_speed )
        _udp_client->setSpeed( _speed );
    else if( _quit )
        _udp_client->stop();
    else if( _help )
    {
        const char* text =
        
        "Help:\n\n"
        "start\ts\tconnect to server, enter ip and port\n"
        "pause\tp\tsuspend the sending of packets\n"
        "stop\t\tstop work\n"
        "speed\t\tset the packs rate\n"
        "skip\t\tmiss a few packs\n"
        "quit\tq\tfinish the work\n"
        "help\th\tdisplay this help\n\n"
        "Press any key...";
        
        _ui->setText( text );
        _ui->switchText();        
    }
}

bool ClientsController::quit() const
{
    return _quit;
}

void ClientsController::setUiState( const char* format, ... )
{
    if( _ui ) 
    {
        va_list args;
        
        va_start( args, format );
        _ui->setState( true, true, format, args );
        va_end( args );
    } 
}

void ClientsController::parse( const char* str )
{
    char prms[ INPUT_SIZE ];
    char args[ MAX_ARGS ][ INPUT_SIZE ];
    
    strcpy( prms, str );
    
    char* pch;
    pch = strtok( prms, " ");
    
    int count = 0;
    for( ; pch != NULL && count < MAX_ARGS; count++ )
    {
        strcpy( args[ count ], pch );
        pch = strtok( NULL, " " );
    }
    
    this->reset();
    
    if     ( ( strcmp( args[0], "help" ) == 0 || strcmp( args[0], "h" ) == 0 ) && count == 1 ) {
        _help = true;
    }
    else if( ( strcmp( args[0], "quit" ) == 0 || strcmp( args[0], "q" ) == 0 ) && count == 1 ) {
        _quit = true;
    }
    else if( ( strcmp( args[0], "start" ) == 0 || strcmp( args[0], "s" ) == 0 ) )
    {
        if     ( count == 1 )
            _start = true;
        else if( count == 2 || count == 3 )
        {
            int a, b, c, d, port; char ch;
            
            int result = sscanf( args[1], "%3u.%3u.%3u.%3u%1c", &a, &b, &c, &d, &ch );
            if( ( result == 4 || ( result == 5 && ch == ' ' ) ) && a < 255 && b < 255 && c < 255 && d < 255 )
            {
                result = sscanf( args[2], "%5u%1c", &port, &ch );
                if( ( result == 1 || ( result == 2 && ch == ' ' ) ) && port < 65535 )
                {
                    _start = true;
                    _address = new Address( (unsigned char)a, (unsigned char)b, (unsigned char)c, (unsigned char)d, (unsigned short) port );                    
                }
                else
                    setUiState( "Error: invalid port\n" );
            }
            else
                setUiState( "Error: invalid ip\n" );
        }
        else
            setUiState( "Enter ip and port of server, example \"start xxx.xxx.xxx.xxx xxxxx\"\n" );
    }
    else if( strcmp( args[0], "pause" ) == 0 && count == 1 ) {
        _pause = true;
    }
    else if( strcmp( args[0], "stop" ) == 0 && count == 1 ) {
        _stop = true;
    }
    else if( strcmp( args[0], "speed" ) == 0 )
    {
        if( count == 2 )
        {
            int tmp_speed;
            int result = sscanf( args[1], "%3u", &tmp_speed );
            if( result == 1 && tmp_speed >= MIN_SPEED && tmp_speed <= MAX_SPEED ) 
            {
                _set_speed = true;
                _speed = tmp_speed;
            }
            else
                setUiState( "Error: invalid speed value (%d-%d)", MIN_SPEED, MAX_SPEED );
        }
        else 
            setUiState( "Enter speed value (%d-%d mbit/sec) \"speed xxx\"\n", MIN_SPEED, MAX_SPEED );
            
    }
    else if( strcmp( args[0], "skip" ) == 0 && ( count == 1 || count == 2 ) ) 
    {
        if( count == 2 )
        {
            int num;
            int result = sscanf( args[1], "%3u", &num );
            if( result == 1 && num >= MIN_SKIP_NUM && num <= MAX_SKIP_NUM )
            {
                _skip = true;
                _skip_number = num;
            }
            else
                setUiState( "Error: invalid packs number (%d-%d)", MIN_SKIP_NUM, MAX_SKIP_NUM );
        }
        else
        {
            _skip = true;
            _skip_number = 1;
        }
    }
    else
        setUiState( "error: invalid arguments, enter \"help\"\n" );
}

void ClientsController::reset()
{
    _help = _quit = _start = _pause = _stop = _set_speed = _skip = false;
    _speed = _skip_number = 0;
    
    _address = nullptr;
}