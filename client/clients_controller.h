

#ifndef _CLIENTS_CONTROLLER_H_
#define _CLIENTS_CONTROLLER_H_


#include "udp_client.h"
#include "address.h"
#include "controller.h"


#define MAX_ARGS 3


class Address;


class ClientsController: public Controller
{
    
public:
    ClientsController( Timers* timers );
    ~ClientsController();
    
    void setUi( UserInterface* ui ) override;
    void run( const char* cmds ) override;
    bool quit() const override;
    UdpClient* model() const { return _udp_client; }
    
private:
    void setUiState( const char* format, ... );
    void parse( const char* str );
    void reset();
    
    UserInterface* _ui;
    UdpClient* _udp_client;
    Address* _address;
    bool _help, _quit, _start, _pause, _stop, _set_speed, _skip, _set_timeout;
    int _speed, _skip_number, _timeout; 
    
};


#endif // _CLIENTS_CONTROLLER_H_
