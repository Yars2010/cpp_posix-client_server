

#include "ui.h"
#include "udp_client.h"
#include "udp_socket.h"
#include "lock_guard.h"
#include "address.h"
#include "functions.h"
#include "timers.h"
#include <time.h>
#include <unistd.h>
#include <string.h>
#include <limits.h>
#include <stdio.h>
#include <errno.h>
#include <sys/time.h>
#include <math.h>
#include <stdarg.h>


class IntQueue
{
public:
    struct Item
    {
        int value;
        Item* next;
    };
    
    IntQueue();
    ~IntQueue();
    
    void pushBack( int value );
    int popFront( );
    int size() const;
    void clear();

private:
    Item *_head, *_tail;
    int _size;
    
};


IntQueue::IntQueue()
    : _head( nullptr ), _tail( nullptr ), _size(0)
{
}


IntQueue::~IntQueue()
{
    clear();
}


void IntQueue::pushBack( int value )
{
    if( _tail != NULL)
    {
        Item *temp = new Item();
        
        temp->value = value;
        temp->next = nullptr;
        
        _tail->next = temp;
        _tail = temp;
    }
    else
    {
        _tail = new Item();
        
        _tail->value = value;
        _tail->next = nullptr;
        
        _head = _tail;
    }
    ++_size;
}


int IntQueue::popFront( )
{
    if ( _head != NULL )
    {
        int value = _head->value;
        if( _head == _tail )
            _tail = NULL;
        
        Item* tmp = _head;
        
        _head = _head->next;
        --_size;
        
        delete tmp;
        
        return value;
    }
    else
        return -1;
}

void IntQueue::clear()
{
    while( _size > 0 ){
        popFront();
    }    
}


int IntQueue::size() const
{
    return _size;
}



UdpClient::UdpClient( Timers* timers)
    :   _socket( nullptr ), 
        _address( nullptr ), 
        _ui( nullptr ),  
        _user_start(false), 
        _user_stop(false), 
        _user_play(false), 
        _user_pause(false), 
        _user_skip(false), 
        _user_speed(false), 
        _user_timeout(false),
        _start(false), 
        _pause(false),
        _begin_check_link(false), 
        _link_ok(true), 
        _begin_check_connect(false), 
        _connect_ok(false),
        _sent_ok(false),
        _check_link(0), 
        _check_speed(0), 
        _check_connect(0),
        _timeout( TIMEOUT ), 
        _speed( SPEED ),
        _data_size(MIN_DATA_SIZE),
        _send_interval(MAX_SEND_INTERVAL), 
        _total_bytes(0),
        _size_step(0),
        _curr_speed(0),
        _last_diff(0),
        _correct_interval(true),
        _curr_speed_more(false), 
        _curr_seed_less(false),
        _pack(0),
        _repeat_packs(nullptr),
        _timers(timers),
        _speed_timer(nullptr), 
        _connect_timer(nullptr), 
        _link_timer(nullptr),
        _val1(new int(1)), 
        _val2(new int(2)), 
        _val3(new int(3)),
        _val4(new int(4)),
        _lastcall_time({0, 0})
{
    
    pthread_mutex_init( &_lock, NULL );
    pthread_cond_init( &_ready, NULL );
    
    sigemptyset( &_block_mask );
    sigaddset( &_block_mask, SIGUSR2 );
    sigaddset( &_block_mask, SIGVTALRM );
    
    sigfillset( &_select_mask );
    sigdelset( &_select_mask, SIGUSR2 );
    sigdelset( &_select_mask, SIGVTALRM );
    
    _socket = new UdpSocket();
    _socket->setType( SocketType::nonblock ); 
    
    _repeat_packs = new IntQueue();
    
    if( pthread_create( &_tid1, NULL, &clientWorkFunc, this ) > 0 )
        exit(1);
}


UdpClient::~UdpClient()
{
    pthread_cancel( _tid1 );
    pthread_join( _tid1, NULL );
    
    clearTalkResource();
    clearTimers();
    
    delete _repeat_packs;
    _repeat_packs = nullptr;
    
    delete _socket;
    _socket = nullptr;
    
    pthread_mutex_destroy( &_lock );
    pthread_cond_destroy( &_ready );
    
    delete _val1;
    delete _val2;
    delete _val3;
    delete _val4;
    _val1 = _val2 = _val3 = _val4 = nullptr;    
}


void UdpClient::setUi( UserInterface* ui )
{
    if( ui )
        _ui = ui;
    else
        exit( 1 );
}


UserInterface* UdpClient::ui() const
{
    return _ui;
}


bool UdpClient::start( Address* address )
{
    LockGuard lock( &_lock );
    
    if( _start && _pause && ! address )
    {
        _user_play = true;
        pthread_kill( _tid1, SIGUSR2 );
    }
    else if( ! _start && address )
    {
        _address = address;
        _user_start = true;
        pthread_kill( _tid1, SIGUSR2 );
    }
    else
        return false;
    
    return true;
}


bool UdpClient::isStart() const
{
    LockGuard lock( &_lock );
    return _start;
}


void UdpClient::pause()
{
    LockGuard lock( &_lock );
    if( _start && ! _pause )
    {
        _user_pause = true;
        pthread_kill( _tid1, SIGUSR2 );
    }
}


bool UdpClient::isPause() const
{
    LockGuard lock( &_lock );
    return _pause;
}


void UdpClient::stop()
{
    LockGuard lock( &_lock );
    if( _start )
    {
        _user_stop = true;
        pthread_kill( _tid1, SIGUSR2 );
    }
}


void UdpClient::skip( int skip_number )
{
    LockGuard lock( &_lock );
    if( _start && ! _pause )
    {
        if( skip_number >= MIN_SKIP_NUM && skip_number <= MAX_SKIP_NUM )
            _skip_number = skip_number;
        else if( skip_number > MAX_SKIP_NUM )
            _skip_number = MAX_SKIP_NUM;
        else
            _skip_number = MIN_SKIP_NUM;
        
        _user_skip = true;
        pthread_kill( _tid1, SIGUSR2 );
    }
}

void UdpClient::setTimeout( int msec_timeout )
{
    LockGuard lock( &_lock );
    
    if( msec_timeout >= MIN_TIMEOUT && msec_timeout <= MAX_TIMEOUT )
        _timeout = msec_timeout;
    else if( msec_timeout > MAX_TIMEOUT )
        _timeout = MAX_TIMEOUT;
    else
        _timeout = MIN_TIMEOUT;
    
    _user_timeout = true;
    pthread_kill( _tid1, SIGUSR2 );
    
}


int UdpClient::timeout() const
{
    LockGuard lock( &_lock );
    return _timeout;
}


void UdpClient::setSpeed( int speed )
{
    LockGuard lock( &_lock );
    
    if( speed >= MIN_SPEED && speed <= MAX_SPEED )
        _speed = speed;
    else if( speed > MAX_SPEED )
        _speed = MAX_SPEED;
    else 
        _speed = MIN_SPEED;
        
    _user_speed = true;
    pthread_kill( _tid1, SIGUSR2 );
}



void UdpClient::setUiState( bool priority, bool flash, const char* format, ...  )
{
    if( _ui ) 
    {
        va_list args;
        
        va_start( args, format );
        _ui->setState( priority, flash, format, args );
        va_end( args );
    }
}


void UdpClient::clearTalkResource()
{
    _speed_timer->stop();
    _connect_timer->stop();
    _link_timer->stop();
    
    _repeat_packs->clear();
    
    _pack = 0;
    _link_ok = true;
    _begin_check_link = _begin_check_connect = _connect_ok =  _sent_ok = false;
    _start = _pause = false;
    _check_link = _check_speed = _check_connect = 0;
    
    _socket->disconnect();
    
    delete _address;
    _address = nullptr;    
}


bool UdpClient::socketConnect()
{
    sigset_t mask;
    sigfillset( &mask );
    
    _socket->connect( _address );
    return _socket->waitConnect( &mask, _timeout ); 
}


int UdpClient::waitReceiveAndSignal( char* in_buff, int in_len )
{
    sigset_t old_mask;
    pthread_sigmask( SIG_BLOCK, &_block_mask, &old_mask );
    
    int result = -1;
    while( true )
    {
        LockGuard lock ( &_lock );
        if     ( _user_start && ! _start )
        {
            _user_start = false;
            
            if( ! this->socketConnect() )
                exit(1);     
            
            _sent_ok = send( "start", 6, _timeout ) > 0;
            if( ! _sent_ok ) {
                break;
            }
            _connect_timer->play( _timeout );
            _begin_check_connect = true;
            _start = true;
            
            setUiState( true, false, "Connecting..." );
        }
        else if( _user_stop && _start )
        {
            _user_stop = false;
            _send_interval = -1;
            clearTalkResource();
            setUiState( true, true, "Stopped" );
        }
        else if( _user_pause && ! _pause )
        {
            _user_pause = false;
            _pause = true;
            
            _send_interval = -1;
            _speed_timer->stop();
            
            setUiState( true, false, "Pause" );
        }
        else if( _user_play && _pause )
        {
            _user_play = false;
            _pause = false;
            
            calcSizeAndInterval();
            _speed_timer->play( 1000, true );
            
            setUiState( true, true, "Play" );
        }
        else if( _user_skip )
        {
            _user_skip = false;
            _pack += _skip_number;
            setUiState( true, true, "Skip %d packs", _skip_number );
        }
        else if( _user_speed )
        {
            _user_speed = false;
            calcSizeAndInterval();
            setUiState( true, true, "Set new speed %d Mbit/sec", _speed );
        }
        else if( _user_timeout )
        {
            _user_timeout = false;
            setUiState( true, true, "Set new timeout %d sec", _timeout );
        }
        else if( _check_link )
        {
            _check_link = 0;
            if( _begin_check_link )
            {
                _link_ok = false;
                break;
            }
            _sent_ok = send( "servavail", 10, _timeout ) > 0;
            if( ! _sent_ok ) {
                break;
            }
            _begin_check_link = true;
        }
        else if( _check_speed )
        {
            _check_speed = 0;
            calcSpeed();
            correctSpeedParams();
            setUiState( false, true, "speed = %5.3f", _curr_speed );
            
        }
        else if( _check_connect )
        {
            _check_connect = 0;
            if( _begin_check_connect )
            {
                _connect_ok = false;
                break;
            }
        }
        lock.unlock();
        
        result = receive( in_buff, in_len, _send_interval );
        if( result >= 0) {
            break;
        }
    }
    
    pthread_sigmask( SIG_SETMASK, &old_mask, NULL );
    return result;
}


int UdpClient::send( const char* out_buff, int len, int timeout )
{
    int data_size = _socket->send( out_buff, len );
    int result = _socket->waitSend( &_select_mask, timeout );
    if( result <= 0 ) {
        return result;
    }
    return data_size;
}


int UdpClient::receive( char* in_buff, int len, int timeout )
{
    int result = _socket->waitRecv( &_select_mask, timeout );
    if( result <= 0 ) {
        return result;
    }
    int data_size = _socket->receive( in_buff, len );
    return data_size;
}


void UdpClient::calcSpeed()
{
    if( _total_bytes > 0 )
    {
        timespec mt;
        clock_gettime (CLOCK_REALTIME, &mt); // reentrancy
        
        if( _lastcall_time.tv_sec == 0 && _lastcall_time.tv_nsec == 0 ) 
        {
            _lastcall_time = mt;
            _total_bytes = 0;
            return;
        }
        
        double time = (double) ( 1000000000 * ( mt.tv_sec - _lastcall_time.tv_sec ) + ( mt.tv_nsec - _lastcall_time.tv_nsec ) ) / 1000000000;
        _lastcall_time = mt;

        _curr_speed = (double) _total_bytes * 8.0 / 1000000.0 / time; // megabit/sec
        _total_bytes = 0;
    }
}


void timersHandler( void* arg1, void* arg2 )
{
    UdpClient* client = (UdpClient*) arg1; // current time don't call "new"
    int* val = (int*) arg2;
    if     ( *val == 1 ) 
        client->_check_connect = 1;
    else if( *val == 2 )
        client->_check_speed = 1;
    else if( *val == 3 )
        client->_check_link = 1;
    else
        exit( 1 );
}


void clientHandler( int signo )
{
    if( signo == SIGUSR2 )
        return;
    else
        exit( 1 );
}


void UdpClient::clearTimers()
{
    _timers->removeTimer( _connect_timer );
    _timers->removeTimer( _speed_timer );
    _timers->removeTimer( _link_timer );
    
    _connect_timer = _speed_timer = _link_timer = nullptr;   
}

void UdpClient::initThread()
{
    sigset_t mask;
    sigemptyset( &mask );
    sigaddset( &mask, SIGUSR2 );
    
    struct sigaction act;
    memset( &act, 0, sizeof(act) );
    act.sa_handler = clientHandler;
    act.sa_mask = mask;
    sigaction( SIGUSR2, &act, 0 );
    
    _connect_timer = _timers->addTimer( timersHandler, (void*)this, (void*)_val1 );
    _speed_timer = _timers->addTimer( timersHandler, (void*)this, (void*)_val2 );    
    _link_timer = _timers->addTimer( timersHandler, (void*)this, (void*)_val3 );
}

/*
double UdpClient::calcSendInterval()
{
    return _data_size / (_curr_speed * 125000) * 1000; 
}


double UdpClient::calcDataSize()
{
    return _curr_speed * (_send_interval / 1000) * 125000;
}
*/

bool UdpClient::incDataSize( int step )
{
    if( _data_size >= MAX_DATA_SIZE ) {
        return false;
    }
    _data_size += step;
    if( _data_size > MAX_DATA_SIZE) {
        _data_size = MAX_DATA_SIZE;
    }
    return true;
}


bool UdpClient::decInterval( int step )
{
    if( _send_interval <= MIN_SEND_INTERVAL ){
        return false;
    }
    _send_interval -= step;
    if( _send_interval < MIN_SEND_INTERVAL ) {
        _send_interval = MIN_SEND_INTERVAL;
    }
    return true;
}


bool UdpClient::decDataSize( int step )
{
    if( _data_size <= MIN_DATA_SIZE ) {
        return false;
    }
    _data_size -= step;
    if( _data_size < MIN_DATA_SIZE ) {
        _data_size = MIN_DATA_SIZE;
    }
    return true;
}


bool UdpClient::incInterval( int step )
{
    if( _send_interval >= MAX_SEND_INTERVAL ) {
        return false;
    }
    _send_interval += step;
    if( _send_interval > MAX_SEND_INTERVAL ) {
        _send_interval = MAX_SEND_INTERVAL;
    }
    return true;
}


void UdpClient::correctSpeedParams()
{
    double diff = 0;
    
    if( _speed > _curr_speed )
    {
        diff = (double) _speed - _curr_speed;
        if( diff < 0.1 )
            return;
        
        if( _correct_interval ){
            decInterval( SEND_INTERVAL_STEP );
        }
        else
        {
            if( _curr_speed_more )
            {
                _size_step /= 2;
                if( _size_step < MIN_SIZE_STEP ) {
                    _size_step = MIN_SIZE_STEP;
                }
                _curr_speed_more = false;
            } 
            incDataSize( _size_step );
        }
        if( ! _curr_seed_less )
            _curr_seed_less = true;
    }
    else
    {
        diff = _curr_speed - (double) _speed;
        if( ( _last_diff == 0 || diff < _last_diff ) && _correct_interval )
        {
            _last_diff = diff;
            incInterval( SEND_INTERVAL_STEP );
        }
        else
        {
            if( _correct_interval )
                _correct_interval = false;
            
            if( _curr_seed_less )
            {                
                _size_step /= 2;
                if( _size_step < MIN_SIZE_STEP ) {
                    _size_step = MIN_SIZE_STEP;
                }
                _curr_seed_less = false;
            }

            decDataSize( _size_step );
        }
        if( ! _curr_speed_more )
            _curr_speed_more = true;
    }
}


void UdpClient::calcSizeAndInterval()
{
    int size = (double) _speed * ( (double) MAX_SEND_INTERVAL / 1000.0 ) * 125000.0;
    if( size > MAX_DATA_SIZE )
    {
        _data_size = MAX_DATA_SIZE;
        int interval = (double) _data_size / ( (double) _speed * 125000.0 ) * 1000.0;
        if( interval < MIN_SEND_INTERVAL )
            _send_interval = MIN_SEND_INTERVAL;
        else
            _send_interval = interval;
    }
    else
    {
        if( size >= MIN_DATA_SIZE )
            _data_size = size;
        else
            _data_size = MIN_DATA_SIZE;
            
        _send_interval = MAX_SEND_INTERVAL;
    }
    
    _last_diff = 0.0;    
    _size_step = MAX_SIZE_STEP;
    
    _curr_speed_more = _curr_seed_less = false;
    _correct_interval = true;
}


bool UdpClient::talking()
{
    this->initThread();
    
    char outbuff[ MAX_DATA_SIZE ];    
    
    int buff_size = 1024;
    char inbuff [ buff_size ];
    
    _send_interval = -1;
    while( true )
    {
        int len = 0;
        while( ( len = waitReceiveAndSignal( inbuff, buff_size ) ) > 0 )
        {
            if( _begin_check_connect && len == 8 && memcmp( inbuff, "startok", 7 ) == 0 )
            {
                _sent_ok = send( "startok", 8, timeout() ) > 0;
                if( ! _sent_ok ) {
                    break;
                }
                _begin_check_connect = false;
                _connect_ok = true;
                _connect_timer->stop();
                
                setUiState( true, true, "Server connection!" );
                
                _speed_timer->play( 1000, true );
                _link_timer->play( timeout(), true );
                
                calcSizeAndInterval();
            }
            else if( _link_ok && _connect_ok )
            {
                if( len >= 8 && memcmp( inbuff, "repeat", 6 ) == 0 )
                {
                    char* pch;
                    pch = strtok( inbuff + 7, " ");
                    
                    int pack = 0;
                    while( pch != NULL && _repeat_packs->size() < REPEAT_SIZE )
                    {
                        int result = sscanf( pch, "%d", &pack );
                        if( result == 1 ) {
                            _repeat_packs->pushBack( pack );
                        }
                        pch = strtok( NULL, " " );
                    }
                }
                else if( len == 10 && memcmp( inbuff, "clntavail", 9 ) == 0 )
                {
                    _sent_ok = send( "clntavail", 10, timeout() ) > 0;
                    if( ! _sent_ok ) {
                        break;
                    }                 
                }
                else if( len == 10 && memcmp( inbuff, "servavail", 9 ) == 0 ) 
                {
                    _begin_check_link = false;
                    _link_ok = true;
                }
                else
                    setUiState( true, true, "Unknown command" );
            }
            else
                setUiState( true, true, "Unknown command" );
        }
        if( _link_ok && _connect_ok && _sent_ok ) 
        {
            if( _pack == LONG_MAX ) {
                _pack = 0;
            }
            if  ( _repeat_packs->size() == 0 )
                sprintf( outbuff, "%ld", ++_pack );
            else
                sprintf( outbuff, "%d", _repeat_packs->popFront() );
                
            _sent_ok = send( outbuff, _data_size, _timeout ) > 0;
            if( _sent_ok ) {
                _total_bytes += _data_size;
            }
        }
        
        if( ! _link_ok || ! _connect_ok || ! _sent_ok ) 
        {
            clearTalkResource();
            _send_interval = -1;
            
            if     ( ! _sent_ok )
                setUiState( true, false, "Error: no connection" );
            else if( ! _connect_ok )
                setUiState( true, false, "Error: client is not started" );
            else if( ! _link_ok )
                setUiState( true, false, "Error: server is unavailable" );
        }
        
    }
    return true;
}


void* clientWorkFunc( void* arg )
{
    UdpClient* client = (UdpClient*) arg;
    client->talking();
    return (void*) 0;
}