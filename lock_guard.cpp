

#include "lock_guard.h"


LockGuard::LockGuard( pthread_mutex_t* mutex, GuardType type )
    : _mutex( mutex ), _type( type ), _lock( false )
{
    if ( type == GuardType::Lock ) {
        lock();
    }
}

LockGuard::~LockGuard()
{
    if( _lock )
        unlock();
}

bool LockGuard::tryLock()
{
    _lock = ( pthread_mutex_trylock( _mutex ) == 0 );
    return _lock;
}

void LockGuard::lock()
{
    pthread_mutex_lock( _mutex );
    _lock = true;
}

void LockGuard::unlock()
{
    pthread_mutex_unlock( _mutex );
    _lock = false;
}

bool LockGuard::isLock()
{
    return _lock;
}