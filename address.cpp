
#include "address.h"
#include <string.h>
#include <stdio.h>
#include <arpa/inet.h>



Address::Address()
{
    _str_addr = new char[ ADDRLEN ];
    memset( _str_addr, ' ', ADDRLEN );
    _str_addr[0] = '\0';
}


Address::Address( u_char a, u_char b, u_char c, u_char d, u_short port )
    : Address()
{
    addressToSockAddr( a, b, c, d, port, &_addr );
}


Address::Address( SystemAddress type )
    : Address()
{
    _addr.sin_family = AF_INET;
    if     ( type == SystemAddress::Loopback )
        _addr.sin_addr.s_addr = htonl( INADDR_LOOPBACK );
    else if( type == SystemAddress::Any )
        _addr.sin_addr.s_addr = htonl( INADDR_ANY );
}


Address::Address( sockaddr_in addr )
    : Address()
{
    _addr = addr;
}


Address::~Address()
{
    delete[] _str_addr;
}


Address& Address::operator=( sockaddr_in addr )
{
    _addr = addr;
    return *this;
}


Address::operator sockaddr_in () const
{
	return _addr;
}


void Address::readAddr()
{
    int str_len = 7;
    char portstr[str_len];
    memset( portstr, ' ', str_len );
    
    inet_ntop( AF_INET, &_addr.sin_addr, _str_addr, ADDRLEN );
    snprintf( portstr, sizeof( portstr ), " %d", ntohs(_addr.sin_port) );
    strcat( _str_addr, portstr );
}

const char* Address::str()
{
    if( strlen( _str_addr ) == 0 )
        readAddr();
        
    return _str_addr;
}


void Address::addressToSockAddr( u_char a, u_char b, u_char c, u_char d, u_short port, sockaddr_in* addr ) const
{
	unsigned int addr_int = ((unsigned int) a << 24) | ((unsigned int) b << 16) | ((unsigned int) c << 8) | (unsigned int) d;
    addr->sin_family = AF_INET;
	addr->sin_addr.s_addr = htonl( addr_int );
	addr->sin_port = htons( port );    
}


bool Address::operator ==( const Address& address ) const
{
    return _addr.sin_family == address._addr.sin_family && _addr.sin_port == address._addr.sin_port && 
        _addr.sin_addr.s_addr == address._addr.sin_addr.s_addr;
}