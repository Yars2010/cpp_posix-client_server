

#include "timers.h"
#include <stdio.h>
#include <unistd.h>
#include <time.h>
#include <sys/time.h>


int count = 0;
bool run = true;
timespec _lastcall_time_1, _lastcall_time_2;


void func( void* arg1, void* arg2 )
{
    int* val = (int*) arg1;
    
    timespec mt;
    clock_gettime (CLOCK_REALTIME, &mt); // reentrancy
    
    double time = 0;
    if( *val == 1 )
    {
        if( _lastcall_time_1.tv_sec != 0 || _lastcall_time_1.tv_nsec != 0 )
            time = (double) ( 1000000000 * ( mt.tv_sec -_lastcall_time_1.tv_sec ) + ( mt.tv_nsec - _lastcall_time_1.tv_nsec ) ) / 1000000000;        
        
        _lastcall_time_1 = mt;
        printf( "good morning, i'm alarm 1!");
    }
    else
    {
        if( _lastcall_time_2.tv_sec != 0 || _lastcall_time_2.tv_nsec != 0 )
            time = (double) ( 1000000000 * ( mt.tv_sec -_lastcall_time_2.tv_sec ) + ( mt.tv_nsec - _lastcall_time_2.tv_nsec ) ) / 1000000000;        
        
        _lastcall_time_2 = mt;
        printf( "good evening, i'm alarm 2!");
    }
    
    printf( "time = %f\n", time );
    ++count;
    
    if( count >= 1000 )
        run = false;
}


int main(int argc, char **argv)
{
    Timers* timers = Timers::instance();
    int *val_1 = new int(1), *val_2 = new int(2);
    Timer* timer1 = timers->addTimer( 5000, true, func, val_1, NULL );
    Timer* timer2 = timers->addTimer( 2000, true, func, val_2, NULL );
    
    timer1->play();
    timer2->play();
    while( run ) {
        pause();
    }
    
	printf("hello world\n");
	return 0;
}
