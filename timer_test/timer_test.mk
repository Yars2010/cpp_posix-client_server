##
## Auto Generated makefile by CodeLite IDE
## any manual changes will be erased      
##
## Debug
ProjectName            :=timer_test
ConfigurationName      :=Debug
WorkspacePath          :=/home/silverboy/projects/cpp_posix-client_server
ProjectPath            :=/home/silverboy/projects/cpp_posix-client_server/timer_test
IntermediateDirectory  :=./Debug
OutDir                 := $(IntermediateDirectory)
CurrentFileName        :=
CurrentFilePath        :=
CurrentFileFullPath    :=
User                   :=silverboy
Date                   :=07/11/19
CodeLitePath           :=/home/silverboy/.codelite
LinkerName             :=g++
SharedObjectLinkerName :=g++ -shared -fPIC
ObjectSuffix           :=.o
DependSuffix           :=.o.d
PreprocessSuffix       :=.o.i
DebugSwitch            :=-gstab
IncludeSwitch          :=-I
LibrarySwitch          :=-l
OutputSwitch           :=-o 
LibraryPathSwitch      :=-L
PreprocessorSwitch     :=-D
SourceSwitch           :=-c 
OutputFile             :=$(IntermediateDirectory)/$(ProjectName)
Preprocessors          :=
ObjectSwitch           :=-o 
ArchiveOutputSwitch    := 
PreprocessOnlySwitch   :=-E 
ObjectsFileList        :="timer_test.txt"
PCHCompileFlags        :=
MakeDirCommand         :=mkdir -p
LinkOptions            :=  
IncludePath            :=  $(IncludeSwitch). $(IncludeSwitch).. 
IncludePCH             := 
RcIncludePath          := 
Libs                   := $(LibrarySwitch)pthread 
ArLibs                 :=  "pthread" 
LibPath                := $(LibraryPathSwitch). 

##
## Common variables
## AR, CXX, CC, AS, CXXFLAGS and CFLAGS can be overriden using an environment variables
##
AR       := ar rcus
CXX      := g++
CC       := gcc
CXXFLAGS :=  -g -O0 -Wall $(Preprocessors)
CFLAGS   :=  -g -O0 -Wall $(Preprocessors)
ASFLAGS  := 
AS       := as


##
## User defined environment variables
##
CodeLiteDir:=/usr/share/codelite
Objects0=$(IntermediateDirectory)/up_lock_guard.cpp$(ObjectSuffix) $(IntermediateDirectory)/up_timers.cpp$(ObjectSuffix) $(IntermediateDirectory)/main.cpp$(ObjectSuffix) $(IntermediateDirectory)/up_functions.cpp$(ObjectSuffix) 



Objects=$(Objects0) 

##
## Main Build Targets 
##
.PHONY: all clean PreBuild PrePreBuild PostBuild MakeIntermediateDirs
all: $(OutputFile)

$(OutputFile): $(IntermediateDirectory)/.d $(Objects) 
	@$(MakeDirCommand) $(@D)
	@echo "" > $(IntermediateDirectory)/.d
	@echo $(Objects0)  > $(ObjectsFileList)
	$(LinkerName) $(OutputSwitch)$(OutputFile) @$(ObjectsFileList) $(LibPath) $(Libs) $(LinkOptions)

MakeIntermediateDirs:
	@test -d ./Debug || $(MakeDirCommand) ./Debug


$(IntermediateDirectory)/.d:
	@test -d ./Debug || $(MakeDirCommand) ./Debug

PreBuild:


##
## Objects
##
$(IntermediateDirectory)/up_lock_guard.cpp$(ObjectSuffix): ../lock_guard.cpp $(IntermediateDirectory)/up_lock_guard.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/home/silverboy/projects/cpp_posix-client_server/lock_guard.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/up_lock_guard.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/up_lock_guard.cpp$(DependSuffix): ../lock_guard.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/up_lock_guard.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/up_lock_guard.cpp$(DependSuffix) -MM ../lock_guard.cpp

$(IntermediateDirectory)/up_lock_guard.cpp$(PreprocessSuffix): ../lock_guard.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/up_lock_guard.cpp$(PreprocessSuffix) ../lock_guard.cpp

$(IntermediateDirectory)/up_timers.cpp$(ObjectSuffix): ../timers.cpp $(IntermediateDirectory)/up_timers.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/home/silverboy/projects/cpp_posix-client_server/timers.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/up_timers.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/up_timers.cpp$(DependSuffix): ../timers.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/up_timers.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/up_timers.cpp$(DependSuffix) -MM ../timers.cpp

$(IntermediateDirectory)/up_timers.cpp$(PreprocessSuffix): ../timers.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/up_timers.cpp$(PreprocessSuffix) ../timers.cpp

$(IntermediateDirectory)/main.cpp$(ObjectSuffix): main.cpp $(IntermediateDirectory)/main.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/home/silverboy/projects/cpp_posix-client_server/timer_test/main.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/main.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/main.cpp$(DependSuffix): main.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/main.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/main.cpp$(DependSuffix) -MM main.cpp

$(IntermediateDirectory)/main.cpp$(PreprocessSuffix): main.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/main.cpp$(PreprocessSuffix) main.cpp

$(IntermediateDirectory)/up_functions.cpp$(ObjectSuffix): ../functions.cpp $(IntermediateDirectory)/up_functions.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/home/silverboy/projects/cpp_posix-client_server/functions.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/up_functions.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/up_functions.cpp$(DependSuffix): ../functions.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/up_functions.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/up_functions.cpp$(DependSuffix) -MM ../functions.cpp

$(IntermediateDirectory)/up_functions.cpp$(PreprocessSuffix): ../functions.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/up_functions.cpp$(PreprocessSuffix) ../functions.cpp


-include $(IntermediateDirectory)/*$(DependSuffix)
##
## Clean
##
clean:
	$(RM) -r ./Debug/


