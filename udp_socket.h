

#ifndef _UDP_SOCKET_H_
#define _UDP_SOCKET_H_


#include <sys/select.h>


class Address;

enum SocketType { block, nonblock };

class UdpSocket
{
public:

	UdpSocket();
	~UdpSocket();
    
    void setType( SocketType type );
    bool waitConnect( sigset_t* mask, int timeout_msec = -1 );
    bool waitBind( sigset_t* mask, int timeout_msec = -1 );
	bool connect( Address* addr );
	void disconnect();    
    bool bind( Address* addr );
    void unbind();
    bool isConnect() const;
    bool isBind() const;
    int waitRecv( sigset_t* mask, int timeout_msec = -1 );
    int waitSend( sigset_t* mask, int timeout_msec = -1 );    
	int send( const char* data, int data_len, const Address* addr = nullptr );
	int receive( char* data, int data_len, Address* addr = nullptr );


private:
    fd_set getSet() const;
    
    SocketType _type;
    bool _is_bind, _is_connect;
	int _sockfd;

};

#endif // _UDP_SOCKET_H_