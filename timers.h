

#ifndef _TIMERS_H_
#define _TIMERS_H_

#include <pthread.h>
#include <signal.h>


#define MAX_TIMERS_NUMBER 10


typedef void (*handler)( void*, void* );


class Timers;
class TimerQueue; 


class Timer
{
    friend class Timers;    
    
    Timer( Timers* timers, handler func, void* arg1, void* arg2 );
    Timer( const Timer& timer ) = default;
    Timer& operator=( const Timer& timer ) = default;
    
public: 
    ~Timer();
    bool isStop() const;
    bool isPlay() const;
    int id() const;
    void setInterval( int msec_interval );
    int interval() const;
    void play( int msec_interval, bool repeat = false );
    void pause();
    void stop();

    
private:
    void runHandler();

    static int _count;
    Timers* _timers;
    int _id;
    volatile sig_atomic_t _handled;
    bool _handling, _deleted;
    
    long _interval, _value;
    bool _repeat, _play;
    handler _func;
    void *_arg1, *_arg2;
 
    sigset_t _mask;
    pthread_t _tid;
};


void* timersWorkFunc( void* arg );
void alrmFunc( int signo );
//void insideFunc( int signo );


const int MSEC_TIME_INTERVAL = 10;


class Timers
{
    Timers();
    ~Timers();
    Timers( const Timers& );
    Timers& operator =( const Timers& );
    
    static void cleanUp();
    
public:
    static Timers* instance();

    Timer* addTimer( handler func, void* arg1, void* arg2 );
    Timer* timer( int index ) const;
    int timersNumber() const;
    void removeTimer( Timer* timer );
    void removeAll();
    
private:
    friend void* timersWorkFunc( void* arg );
    friend void alrmFunc( int signo );
    friend class Timer;
    //friend void insideFunc( int signo );
    
    void runHandler();
    int find( int id ); 
    void remove( int index );
    void handlingTimers();
    
    void startAlrmSignal();
    void stopAlrmSignal();

    static Timers* _instance;
    Timer **_timers;
    int _number;
    volatile sig_atomic_t _timer_alarm;
    Timer* _current_timer;
    bool _alrm_signal;
    
    TimerQueue* _queue;
    pthread_t _tid;
    sigset_t _block_mask;
    mutable pthread_mutex_t _mutex;
    
};

#endif // _TIMERS_H_