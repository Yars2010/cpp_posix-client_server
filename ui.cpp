

#include "ui.h"
#include "lock_guard.h"
#include "controller.h"
#include "timers.h"
#include "functions.h"

#include <ncurses.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <sys/ioctl.h>


#ifdef DEBUG
    extern FILE* debug_file;
#endif


UserInterface::UserInterface( Controller* controller, Timers* timers )
    :   _controller(nullptr), 
        _timers(nullptr),
        _priority_timer(nullptr),
        _regular_timer(nullptr),
        _val1(new int(1)),
        _val2(new int(2)),
        _name(nullptr), 
        _input(nullptr), 
        _priority_state(nullptr), 
        _regular_state(nullptr),
        _text(nullptr), 
        _len(0), 
        _pos(0), 
        _win_h(0), 
        _win_w(0), 
        _menu(true), 
        _start(false), 
        _flash_state(false),
        _update_priority(0),
        _update_regular(0)
{
    if( ! controller || ! timers )
        exit( 1 );
    
    initscr();
    raw();
    noecho();
    keypad( stdscr, true );
    pthread_mutex_init( &_lock, NULL );
    
    _controller = controller;
    _controller->setUi( this );    
    _timers = timers;
    
    _name = new char[ NAME_SIZE ];
    _input = new char[ INPUT_SIZE ];
    _priority_state = new char[ STATE_SIZE ];
    _regular_state = new char[ STATE_SIZE ];
    _text = new char[ TEXT_SIZE  ];
    
    *_input = (char) 0;
    
    getmaxyx( stdscr, _win_h, _win_w );
    
    sigemptyset( &_block_mask );
    sigaddset( &_block_mask, SIGUSR1 );
    sigaddset( &_block_mask, SIGWINCH );
    
    if( pthread_sigmask( SIG_BLOCK, &_block_mask, NULL ) != 0 )
        exit( 1 );
        
    if( pthread_create( &_tid, NULL, paintFunc, this ) != 0 )
        exit( 1 );
    
}

UserInterface::~UserInterface()
{
    delete _name;
    delete _input;
    delete _priority_state;
    delete _regular_state;
    delete _text;
    _name = _input = _priority_state = _regular_state = _text = nullptr;
    
    delete _val1;
    delete _val2;
    _val1 = _val2 = nullptr;
    
    if( _start ) {
        _start = false;
    }
    
    echo();
    endwin();
}


void flashHandler( void* arg1, void* arg2 )
{
    UserInterface* ui = (UserInterface*) arg1;
    int* val = (int*) arg2;
    
    if     ( *val == 1 )
        ui->_update_priority = 1;
    else if( *val == 2 )
        ui->_update_regular = 1;
        
    pthread_kill( ui->_tid, SIGUSR1 );
}


void* paintFunc( void* arg )
{
    UserInterface* ui = (UserInterface*) arg;
    
    ui->_priority_timer = ui->_timers->addTimer( flashHandler, (void*)ui, ui->_val1 );
    ui->_regular_timer = ui->_timers->addTimer( flashHandler, (void*)ui, ui->_val2 );
    
    int err, signo;
    while( true )
    {
        err = sigwait( &ui->_block_mask, &signo );
        if( err != 0 )
            exit( 1 );
            
        if ( signo == SIGUSR1 || signo == SIGWINCH )
        {
            if     ( signo == SIGWINCH )
            {
                ui->windowSize( &ui->_win_h, &ui->_win_w );
                resizeterm( ui->_win_h, ui->_win_w );
            }
            else if( ui->_update_priority )
            {
                ui->_update_priority = 0;
                strcpy( ui->_priority_state, "" );
            }
            else if( ui->_update_regular )
            {
                ui->_update_regular = 0;
                strcpy( ui->_regular_state, "" );
            }
            
            LockGuard lock( &ui->_lock );
            if( ui->_menu ) 
                ui->paintMenu();
            else
                ui->paintText();
        }
        else
            exit( 1 );
    }
    return (void*) 0;
}

void UserInterface::start()
{
    LockGuard lock( &_lock );
    if( _start )
        return;
    
    _start = true;
    lock.unlock();
    
    scanInput();
    
    pthread_cancel( _tid );
    pthread_join( _tid, NULL );
}


void UserInterface::switchMenu()
{
    LockGuard lock( &_lock );
    if( ! _menu ) {
        _menu = true;
    }
    update();
}


void UserInterface::switchText()
{
    LockGuard lock( &_lock );
    if( _menu ) {
        _menu = false;
    }
    update();
}


void UserInterface::setName( const char* name )
{
    LockGuard lock( &_lock );
    strcpy( _name, name );
    update();
}


void UserInterface::setState( bool priority, bool flash, const char* format, va_list args )
{
    LockGuard lock( &_lock );
    
    char buffer[ STATE_SIZE ];
    vsprintf( buffer, format, args );

    if( priority )
    {
        strcpy( _priority_state, buffer );
        if( flash )
            _priority_timer->play( 2000 );
        else
            _priority_timer->stop();
    }
    else
    {
        strcpy( _regular_state, buffer );
        if( flash )
            _regular_timer->play( 2000 );
        else
            _regular_timer->stop();
    }
    update();
}


void UserInterface::setText( const char* text )
{
    LockGuard lock( &_lock );
    strcpy( _text, text );
    update();
}


void UserInterface::update()
{
    if( _start ){
        pthread_kill( _tid, SIGUSR1 );
    }       
}


void UserInterface::paintText()
{
    clear();
    mvaddstr( NAME_Y, NAME_X, _text );
    refresh();
}


void UserInterface::paintMenu()
{
    clear();
    mvaddstr( NAME_Y, NAME_X, _name );
    mvaddstr( ARROW_Y, ARROW_X, "> ");
    mvaddstr( INPUT_Y, INPUT_X, _input );
    
    if     ( strlen( _priority_state ) > 0 )
        mvaddstr( stateY(), stateX(), _priority_state );
    else if( strlen( _regular_state ) > 0 )
        mvaddstr( stateY(), stateX(), _regular_state );
        
    move( curY(), curX() );
    refresh();
}


void UserInterface::scanInput()
{
    int ch;
    while(true) 
    {
        LockGuard lock( &_lock );
        if( _menu )
            paintMenu();
        else
            paintText();
        lock.unlock();

        ch = getch();
        
        lock.lock();
        if( _menu )
        {
            lock.unlock();
            
            if     ( ch == '\n' )
            {
                _controller->run( _input );
                memset( _input, 0, strlen(_input ) );
                _pos = _len = 0;
     
                if( _controller->quit() ) 
                    break;
            }
            else if( ch == CTRL('c') )
            {
                _controller->run( "quit" );
                break;
            }
            else if( ch > 31 && ch < 127 && _len < INPUT_SIZE )
            {
                if( _pos == _len )
                {
                    char* curr = _input + _len;
                    *curr = (char) ch;
                    *(curr + 1) = '\0';
                }
                else 
                {
                    char *start = _input + _pos, *end = _input + _len + 1;
                    
                    int size = end - start;
                    char* buff = new char[ size ];
                    
                    memcpy( buff, start, size );
                    memcpy( start + 1, buff, size );
                    
                    delete[] buff;
                    
                    *start = (char) ch;
                }

                ++_pos;
                ++_len;
            }
            else if( ( ( ch == KEY_BACKSPACE || ch == BACKSPACE ) && _pos > 0 ) || ( ch == KEY_DC && _pos < _len ) )
            {
                char* start = _input + _pos, *end = _input + _len + 1;
                int size = end - start;
                char* buff = new char[ size ];
                
                if( ch == KEY_BACKSPACE || ch == BACKSPACE )
                {
                    memcpy( buff, start, size );
                    memcpy( start - 1, buff, size );

                    --_pos;
                }
                else
                {
                    memcpy( buff, start + 1, size - 1 );
                    memcpy( start, buff, size - 1);
                }
                delete[] buff;
                    
                --_len;
            }
            else if( ch == KEY_LEFT )
            {
                if( _pos > 0 )
                    --_pos;
            }
            else if( ch == KEY_RIGHT )
            {
                if( _pos < _len )
                    ++_pos;
            }
            else if( ch == KEY_HOME ) {
                _pos = 0;
            }
            else if( ch == KEY_END ) {
                _pos = _len;
            }
        }
        else {
            _menu = true;
        }
    }
}


void UserInterface::windowSize( int* row, int* col )
{
    struct winsize size;
    if ( ioctl( STDIN_FILENO, TIOCGWINSZ, (char *) &size) < 0 )
        exit( 1 );
    
    *row = size.ws_row;
    *col = size.ws_col;
}


int UserInterface::stateX() const
{
    return STATE_X;
}


int UserInterface::stateY() const
{
    return STATE_Y + ( _len + INPUT_X ) / _win_w;
}


int UserInterface::curX() const
{
    return ( _pos + INPUT_X ) % _win_w;
}


int UserInterface::curY() const
{
    return INPUT_Y + ( _pos + INPUT_X ) / _win_w;
}

