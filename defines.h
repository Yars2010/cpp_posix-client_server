

#ifndef _DEFINES_H_
#define _DEFINES_H_


#define MAX_TIMEOUT 60000
#define MIN_TIMEOUT 250
#define TIMEOUT 2000

#define MAX_SPEED 100
#define MIN_SPEED 1
#define SPEED 8

#define MAX_SKIP_NUM 100
#define MIN_SKIP_NUM 1

#define MIN_DATA_SIZE 508
#define MAX_DATA_SIZE 65507
#define MIN_SIZE_STEP 10
#define MAX_SIZE_STEP 5000

#define MAX_SEND_INTERVAL 500
#define MIN_SEND_INTERVAL 1
#define SEND_INTERVAL_STEP 1

#define REPEAT_SIZE 1000

#endif // _DEFINES_H_