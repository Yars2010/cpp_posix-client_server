

#ifndef _UI_H_
#define _UI_H_


#define NAME_SIZE       256
#define INPUT_SIZE      256
#define STATE_SIZE      32768
#define TEXT_SIZE       32768
#define NAME_Y          0
#define NAME_X          0
#define ARROW_Y         2
#define ARROW_X         0
#define INPUT_Y         2
#define INPUT_X         2
#define STATE_Y         4
#define STATE_X         0
#define BACKSPACE       127


#include <pthread.h>
#include <signal.h>
#include <stdarg.h>


class Controller;
class Timer;
class Timers;


void* paintFunc( void* arg );
void flashHandler( void* arg1, void* arg2 );


class UserInterface
{
public:
    UserInterface( Controller* controller, Timers* timers );
    ~UserInterface();
    
    void start();
    void setName( const char* name );
    void setState( bool priority, bool flash, const char* format, va_list args );
    void setText( const char* text );
    void switchMenu();
    void switchText();
    
private:
    friend void* paintFunc( void* arg );
    friend void flashHandler( void* arg1, void* arg2 );
    
    void update( ); 
    void paintText();
    void paintMenu();
    void scanInput();
    void windowSize( int* x, int* y );
    int stateX() const;
    int stateY() const;
    int curX() const;
    int curY() const;

    Controller* _controller;
    Timers* _timers;
    Timer *_priority_timer, *_regular_timer;
    int *_val1, *_val2;
    char *_name, *_input, *_priority_state, *_regular_state, *_text;
    int _len, _pos, _win_h, _win_w;
    bool _menu, _start, _flash_state;
    volatile sig_atomic_t _update_priority, _update_regular;
    pthread_t _tid;
    sigset_t _block_mask;
    mutable pthread_mutex_t _lock;
};


#endif // _UI_H_