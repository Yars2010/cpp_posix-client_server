

#include "ui.h"

#include <ncurses.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <string.h>
#include <stdlib.h>


UserInterface* UserInterface::_instance = nullptr;


UserInterface* UserInterface::Instance()
{
    if( _instance == nullptr )
        _instance = new UserInterface();
        
    return _instance;
}

UserInterface::UserInterface()
    : _name(nullptr), _input(nullptr), _state(nullptr), _text(nullptr), _len( 0 ), _pos( 0 ), _win_h( 0 ), _win_w( 0 ), _menu( true )
{
    initscr();
    noecho();
    keypad( stdscr, true );
    
    _name = "Client ver. 1.0";
    _input = new char[ INPUT_SIZE ];
    _state = new char[ STATE_SIZE ];
    _text  = new char[ TEXT_SIZE  ];
    
    *_input = (char) 0;
    
    windowSize( &_win_h, &_win_w );
}

UserInterface::~UserInterface()
{
    delete _input;
    delete _state;
    delete _text;
    _input = _state = _text = nullptr;
    
    if( _start ) {
        _start = false;
    }
    endwin();
}

void* paintFunc( void* arg )
{
    UserInterface* ui = (UserInterface*) arg;
    
    int signo;
    while( true )
    {
        if( sigwait( &ui->_mask, &signo ) )
            exit( 1 );
            
        if ( signo == SIGUSR1 || signo == SIGWINCH )
        {
            if( signo == SIGWINCH )
            {
                ui->windowSize( &ui->_win_h, &ui->_win_w );
                resizeterm( ui->_win_h, ui->_win_w );                
            }
            if( _menu )
                ui->paintMenu();
            else
                ui->paintText();
        }
        else if( signo == SIGINT || signo == SIGQUIT )
        {
            
            break;
        }
        else
            exit( 1 );
    }
    return (void*) 0;
}

void UserInterface::start()
{
    sigemptyset( &_mask );
    sigaddset( &_mask, SIGINT );
    sigaddset( &_mask, SIGQUIT );
    sigaddset( &_mask, SIGUSR1 );
    sigaddset( &_mask, SIGWINCH );
    
    if( pthread_sigmask( SIG_BLOCK, &_mask, &_old_mask ) != 0 )
        exit( 1 );
        
    if( pthread_create( &_tid, NULL, paintFunc, this ) != 0 )
        exit( 1 );
        
    _start = true;
    
    scanInput();
    
    if( sigprocmask( SIG_SETMASK, &_old_mask, NULL ) < 0 )
        exit( 1 );
}

void UserInterface::showMain()
{
    if( _start )
    {
        if( !_menu) {
            _menu = true;
        }
        pthread_kill( _tid, SIGUSR1 );
    }
}

void UserInterface::showText( )
{
    if( _start )
    {
        if( _menu ) {
            _menu = false;
        }
        pthread_kill( _tid, SIGUSR1 );
    }
}

void UserInterface::setState( const char* msg )
{
    if( strlen( msg ) <= STATE_SIZE )
        strcpy( _state, msg );
    else
        exit( 1 );
}

void UserInterface::setText( const char* msg )
{
    if( strlen( msg ) <= TEXT_SIZE )
        strcpy( _text, msg );
    else
        exit( 1 );
}

void UserInterface::paintText()
{
    clear();
    
    mvaddstr( NAME_Y, NAME_X, _name );
    
    refresh();
}

void UserInterface::paintMenu()
{
    clear();

    mvaddstr( NAME_Y, NAME_X, _name );
    mvaddstr( ARROW_Y, ARROW_X, "> ");
    mvaddstr( INPUT_Y, INPUT_X, _input );
    mvaddstr( stateY(), stateX(), _state );
    move( curY(), curX() );
    
    refresh();
}

void UserInterface::scanInput()
{
    int ch;
    do {
        ch = mvgetch( curY(), curX() );
        if( ch > 31 && ch < 127 && _len < INPUT_SIZE )
        {
            if( _pos == _len )
            {
                char* curr = _input + _len;
                *curr = (char) ch;
                *(curr + 1) = '\0';
            }
            else 
            {
                char *start = _input + _pos, *end = _input + _len;
                memmove( start + 1, start, end - start );
                *start = (char) ch;
            }
            
            showMain();
            
            ++_pos;
            ++_len;
        }
        else if( ( ( ch == KEY_BACKSPACE || ch == BACKSPACE ) && _pos > 0 ) || ( ch == KEY_DC && _pos < _len ) )
        {
            char* start = _input + _pos, *end = _input + _len;
            if( ch == KEY_BACKSPACE || ch == BACKSPACE )
            {
                memmove( start - 1, start, end - start + 1 );
                --_pos;
            }
            else
                memmove( start, start + 1, end - start );
                
            --_len;
            
            showMain();
        }
        else if( ch == KEY_LEFT )
        {
            if( _pos > 0 )
                --_pos;
        }
        else if( ch == KEY_RIGHT )
        {
            if( _pos < _len )
                ++_pos;
        }
        else if( ch == KEY_HOME ) {
            _pos = 0;
        }
        else if( ch == KEY_END ) {
            _pos = _len;
        }
    } 
    while( ch != '\n' );
}

void UserInterface::windowSize( int* row, int* col )
{
    struct winsize size;
    if ( ioctl( STDIN_FILENO, TIOCGWINSZ, (char *) &size) < 0 )
        printf("error");
    
    *row = size.ws_row;
    *col = size.ws_col;
}

int UserInterface::stateX() const
{
    return STATE_X;
}

int UserInterface::stateY() const
{
    return STATE_Y + ( _len + INPUT_X ) / _win_w;
}

int UserInterface::curX() const
{
    return ( _pos + INPUT_X ) % _win_w;
}

int UserInterface::curY() const
{
    return INPUT_Y + ( _pos + INPUT_X ) / _win_w;
}

