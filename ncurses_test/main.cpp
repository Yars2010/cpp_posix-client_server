
#include <ncurses.h>
#include <stdio.h>
#include <signal.h>
#include <sys/time.h>
#include <string.h>


#include "ui.h"


UserInterface* ui;
int count;

/*
void timer( int signo )
{
    ++count;
    
    char msg[100];
    char num[12];
    sprintf( num, "%d", count );
    
    strcpy( msg, "hello loh count = " );
    strcat( msg, num );
    
    ui->setState( msg );
}
*/

int main( int argc, char **argv )
{
    ui = UserInterface::Instance();
    /*
    signal( SIGALRM, timer );
    
    itimerval tval;
    
    timerclear(&tval.it_interval);
    timerclear(&tval.it_value);
    
    tval.it_interval.tv_sec = 1;
    tval.it_value.tv_sec = 1;

    setitimer( ITIMER_REAL, &tval, NULL );
    */
    
    ui->start();
    delete ui;
    
	return 0;
}
