

#ifndef _UI_H_
#define _UI_H_


#define INPUT_SIZE      256
#define STATE_SIZE      256
#define TEXT_SIZE       512
#define NAME_Y          0
#define NAME_X          0
#define ARROW_Y         2
#define ARROW_X         0
#define INPUT_Y         2
#define INPUT_X         2
#define STATE_Y         4
#define STATE_X         0
#define BACKSPACE       127


#include <pthread.h>
#include <signal.h>


void* paintFunc( void* arg );


class UserInterface
{
    UserInterface();
    
public:
    static UserInterface* Instance();
    ~UserInterface();
    
    void start();
    void showMain( );
    void showText( ); 
    void setState( const char* msg );
    void setText( const char* msg );
    
private:
    friend void resize( int signo );
    friend void* paintFunc( void* arg );
    
    void paintText();
    void paintMenu(); 
    void scanInput();
    void windowSize( int* x, int* y );
    int stateX() const;
    int stateY() const;
    int curX() const;
    int curY() const;

    static UserInterface* _instance;
    const char* _name;
    char* _input;
    char* _state;
    char* _text;
    int _len, _pos, _win_h, _win_w;
    bool _menu, _start;
    pthread_t _tid;
    mutable pthread_mutex_t _lock;
    sigset_t _mask, _old_mask;
    
};


#endif // _UI_H_